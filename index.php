<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>Hotel Alê</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="stylesheet" href="assets/css/main.css">
    <link href="lightbox2-master/dist/css/lightbox.css" rel="stylesheet">
    <script src="dist/js/lightbox-plus-jquery.js"></script> <noscript><link rel="stylesheet"  href="assets/css/noscript.css"></noscript>
	<link rel="stylesheet" href="assets/css/grid.css">
  </head>
  <body class="is-preload">
    <!-- Sidebar -->
    <section id="sidebar">
      <div class="inner">
        <nav>
          <ul>
            <li><a href="#intro">O Hotel</a></li>
            <li><a href="#one">Quartos</a></li>
            <li><a href="#two">Exclusivo para clientes Hosteling</a></li>
            <li><a href="#three">Get in touch</a></li>
          </ul>
        </nav>
      </div>
      <span> </span></section>
    <!-- Wrapper -->
    <div id="wrapper">
      <!-- Intro -->
      <section id="intro" class="wrapper style1 fullscreen fade-up">
        <div class="inner">
			<h1>O Hotel</h1>
			<p>O Hotel Alê tem jeitinho de casa do interior!</p><p> É para quem gosta das coisas simples da vida, como tomar um café, estar entre amigos ou passar uns dias com a família. Fica bem perto de restaurantes, bares, bancos, pontos de ônibus e comércio (inclusive de lingerie). Tem fácil acesso às principais atrações turísticas da cidade.</p>
				<p>Possui quartos de casal, triplo, de familia e idividual.</p>
			<div class="container" style="width: 100%;">
				<div class="itemGrid1"><img src="img/newImg/recepcao.jpeg" style="width: 100%; display: flex; float: left;" /></div>
				<div class="itemGrid2" ><a href="#one" class="button scrolly">Reserve Já</a></div>
			</div>
        </div>
      </section>
      <!-- One -->
      <section style="padding: 0px;" id="one" class="wrapper style2 fullscreen spotlights containerCentralizado">
		<div class="inner itemCentralizado containerCentralizado roundedCorner" style="background-color: rgba(50, 50, 50, 0.5); width: 100%; height: 100%;">
			<div class="itemCentralizado container">
				<div class="itemGrid1">
					<h2>Quartos</h2>
					<div style="display:inline-block;"> 
						<img src="img/newImg/recepcao.jpeg" style="max-width:100%;"> 
					</div>
				</div>
				<div class="itemGrid2">
					<div style="">Os quartos possuem um estilo rústico e aconchegante.
						<ul class="actions">
							<li><a href="generic.php" class="button scrolly">Conheça os quartos</a></li>
						</ul>
					</div>
				</div>
				<div class="fill">&nbsp;</div>
			</div>
		</div>
	</section>
      <!-- Two -->
      <section id="two" class="albergueBackground containerCentralizado wrapper fullscreen fade-up ">
			<div class="containerCentralizado inner itemCentralizado roundedCorner albergueForeground">
				<div class="itemCentralizado container" style=" max-width: 100%;">
					<div class="itemGrid1">
						<h2>É associado da Hostelling International?</h2>
						<div style="display:inline-block;"> <img style="max-width: 100%;" src="img/newImg/recepcao.jpeg"> </div>
					</div>
					<div class="itemGrid2">
						<div><p>Aproveite as tarifas especiais em quartos compartilhados - com beliches de madeira maciça, fabricados especialmente para adultos.</p>
							<ul class="actions">
								<li><a href="#one" class="button scrolly">Reserve Já</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
	  </section>
      <!-- Three -->
      <section id="three" class="wrapper style1 fade-up">
        <div class="inner">
          <h2>Fale Conosco</h2>
          <!--<p>Phasellus convallis elit id ullamcorper pulvinar. Duis aliquam
            turpis mauris, eu ultricies erat malesuada quis. Aliquam dapibus,
            lacus eget hendrerit bibendum, urna est aliquam sem, sit amet
            imperdiet est velit quis lorem.</p>-->
			<br>
          <div class="split style1">
            <section>
              <form method="post" action="enviaremail.php">
                <div class="fields">
                  <div class="field half"> <label for="name">Nome</label> <input name="name" id="name" type="text"> </div>
                  <div class="field half"> <label for="email">Email</label> <input name="email" id="email" type="email"> </div>
                  <div class="field"> <label for="message">Mensagem</label> <textarea name="message" id="message" rows="5"></textarea> </div>
                </div>
                <ul class="actions">
                  <li><a href="" class="button submit">Enviar Mensagem</a></li>
                </ul>
              </form>
            </section>
            <section>
              <ul class="contact">
                <li>
                  <h3>Endereço</h3>
                  <span>Rua Dante Laginestra, 89, Centro<br>
                    Nova Friburgo, RJ 28610-005<br>
                    Brasil</span> </li>
                <li>
                  <h3>Email</h3>
                  <a href="#">hotel.ale@hotmail.com</a> </li>
                <li>
                  <h3>Telefone</h3>
                  <span>(22) 2526-3478</span> </li>
                <li>
                  <h3>Social</h3>
                  <ul class="icons">
                    <li><a href="#" class="fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="#" class="fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="#" class="fa-github"><span class="label">GitHub</span></a></li>
                    <li><a href="#" class="fa-instagram"><span class="label">Instagram</span></a></li>
                    <li><a href="#" class="fa-linkedin"><span class="label">LinkedIn</span></a></li>
                  </ul>
                </li>
              </ul>
            </section>
          </div>
        </div>
      </section>
    </div>
    <!-- Footer -->
    <footer id="footer" class="wrapper style1-alt">
      <div class="inner">
        <ul class="menu">
          <li>© Untitled. All rights reserved.</li>
          <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
        </ul>
      </div>
    </footer>
    <!-- Scripts -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.scrollex.min.js"></script>
    <script src="assets/js/jquery.scrolly.min.js"></script>
    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/util.js"></script>
    <script src="assets/js/main.js"></script>
  </body>
</html>
<!--
	Hyperspace by HTML5 UP	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)-->