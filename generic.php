<!DOCTYPE HTML>
<!--
	Hyperspace by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<title>Hotel Alê</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<link rel="stylesheet" href="assets/css/main.css">
		<link rel="stylesheet" href="assets/css/gridQuarto.css">
		<link href="lightbox/dist/css/lightbox.css" rel="stylesheet">
		<script src="lightbox/dist/js/lightbox-plus-jquery.js"></script>
		<noscript><link rel="stylesheet" href="assets/css/noscript.css"></noscript>
		
	</head>
	<body class="is-preload">

		<!-- Header -->
			<header id="header">
				<a href="index.html" class="title">Hotel Alê</a>
				<nav>
					<ul>
						<li><a href="index.html">Home</a></li>
						<li><a href="generic.html" class="active">Generic</a></li>
						<li><a href="elements.html">Elements</a></li>
					</ul>
				</nav>
			</header>
		<br>
		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<section id="main" class="wrapper">
						<div style="width:100%; padding:0; margin-left:0; margin-right:0;" class="inner">
							<h1 class=" major noMargin">Conheça os quartos</h1>
							<!--<span class="image fit"><img src="img/newImg/QuartoTriplo3.jpg" alt="Quartos" /></span>-->
							<!--<div id="carouselQuartos" class="carousel slide image fit" data-ride="carousel">
								<!--<ol class="carousel-indicators">
									<li data-target="#carouselQuartos" data-slide-to="0" class="active"></li>
									<li data-target="#carouselQuartos" data-slide-to="1"></li>
									<li data-target="#carouselQuartos" data-slide-to="2"></li>
								</ol>
								<div class="carousel-inner">
								<div class="carousel-item active">
								  <img class="d-block w-100" src="img/newImg/QuartoTriplo3.jpg" alt="First slide">
								</div>
								<div class="carousel-item">
								  <img class="d-block w-100" src="img/newImg/QuartoTriplo3.jpg" alt="Second slide">
								</div>
								<div class="carousel-item">
								  <img class="d-block w-100" src="img/newImg/QuartoTriplo3.jpg" alt="Third slide">
								</div>
								</div>
								<a class="carousel-control-prev" href="#carouselQuartos" role="button" data-slide="prev">
									<span class="carousel-control-prev-icon" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
								</a>
								<a class="carousel-control-next" href="#carouselQuartos" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
								</a>
							</div>-->
														
							<div style="width:100%;" class=" wrapper style1 fullscreen  fade-up container">
								
									<div class="gridHeaderQuartos"><h2>Quarto Triplo Especial</h2></div>
									<div class="itemGridQuartos1"><a data-lightbox="triplo" href="img/newImg/QuartoTriplo3.jpg"><img style="width: 100%; display: flex; float: left;" src="img/newImg/QuartoTriplo3.jpg"/></a></div>
									<div class="fill"><p> </p></div>
									<div class="itemGridQuartos2">
										<p style="text-align:justify">Quarto que acomoda até três pessoas. Possui 3 camas de solteiro e banheiro privativo</p>
										<a href="#one" class="button scrolly">Reserve Já</a>
									</div>
								
								
							</div>
							<div  class="wrapper style1 fullscreen fade-up container"><!--  class= "fullscreen"-->
								
								<div class="gridHeaderQuartos"><h2>Quarto de Casal</h2></div>
								<div class="itemGridQuartos1"><img style="width: 100%; display: flex;" src="img/newImg/QuartoCasal (18).jpg"/></div>
								<div class="fill"><p> </p></div>
								<div class="itemGridQuartos2">
									<p style="text-align:justify">Quarto com uma cama de casal, frigobar e amplo
									banheiro com secador de cabelo e box de cortina.</p>
									<a href="#one" class="button scrolly">Reserve Já</a>
								</div>
								
							</div>
							<div style="width:100%;" class=" wrapper style1 fullscreen  fade-up container">
								
									<div class="gridHeaderQuartos"><h2>Quarto Conjugado Especial (Casal + 2)</h2></div>
									<div class="itemGridQuartos1"><img style="width: 100%; display: flex; float: left;" src="img/newImg/QuartoConjugado (5).jpg"/></div>
									<div class="fill"><p> </p></div>
									<div class="itemGridQuartos2">
										<p style="text-align:justify">Quarto para família ou grupo de amigos de até quatro pessoas. Formado por um quarto de casal com banheiro privativo e um 
										quarto com duas camas de solteiro e sacada para a rua.</p>
										<a href="#one" class="button scrolly">Reserve Já</a>
									</div>
								
								
							</div>
							<div style="width:100%;" class=" wrapper style1 fullscreen  fade-up container">
								
									<div class="gridHeaderQuartos"><h2>Quarto de Hostel</h2></div>
									<div class="itemGridQuartos1"><img style="width: 100%; display: flex; float: left;" src="img/newImg/QuartoConjugado (21).jpg"/></div>
									<div class="fill"><p> </p></div>
									<div class="itemGridQuartos2">
										<p style="text-align:justify">Quarto para quem quer uma opção mais econômica de hospedagem.<br>
										Quarto com duas camas de solteiro, ou um beliche de madeira maciça fabricado para grandes adultos.<br>
										Os banheiros ficam no corredor e atendem até 3 quartos.</p>
										<a href="#one" class="button scrolly">Reserve Já</a>
									</div>
								
								
							</div>
						</div>
						
						</div>
					</section>

			</div>

		<!-- Footer -->
			<footer id="footer" class="wrapper alt">
				<div class="inner">
					<ul class="menu">
						<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>
		<!-- Bootstrap-->
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
			<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
			
			
			

	</body>
</html>