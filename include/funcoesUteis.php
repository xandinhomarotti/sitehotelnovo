<?php
function validar($objeto,$pagina=false){
if($pagina){
	if(!session_is_registered($objeto)){
		header("Location: $pagina");
		exit();
	}
	else return true;
}
else if(!session_is_registered($objeto)) return false;
return true;
}

function converteDataUsuario($Data){ // converte no formato do banco
	$d = explode ("/", $Data);//tira a barra
	$rstData = "$d[2]-$d[1]-$d[0]";//separa as datas $d[2] = ano $d[1] = mes etc...
	return $rstData;
}

function converteDataBanco($Data){ // converte no formato de visualiza��o do usuario
	$d = explode ("-", $Data);//tira a barra
	$rstData = "$d[2]/$d[1]/$d[0]";//separa as datas $d[2] = ano $d[1] = mes etc...
	return $rstData;
}

function novoCodigo($tabela,$campo){
	$banco = new BD();
	$busca = "select max(".$campo.") from ".$tabela;
	$resultado = $banco->pesquisarBD($busca);
	$dados = $banco->mostra_registros($resultado);
	return $dados["max($campo)"];
}

function data(){
	$data = getdate();
	switch($data["month"]){
		case "January"  : $mes = "Janeiro";
						break;
		case "February" : $mes = "Fevereiro";
						break;
		case "March"    : $mes = "Mar�o";
						break;
		case "April"    : $mes = "Abril";
						break;
		case "May"      : $mes = "Maio";
						break;
		case "June"     : $mes = "Junho";
						break;
		case "July"	    : $mes = "Julho";
						break;
		case "August"   : $mes = "Agosto";
						break;
		case "September": $mes = "Setembro";
						break;
		case "October"  : $mes = "Outubro";
						break;
		case "November" : $mes = "Novembro";
						break;
		case "December" : $mes = "Dezembro";
						break;
    }
	switch($data["weekday"]){
		case "Sunday"  : $dia = "Dom";
						break;
		case "Monday" : $dia = "Seg";
						break;
		case "Tuesday"    : $dia = "Ter";
						break;
		case "Wednesday"    : $dia = "Qua";
						break;
		case "Thursday"      : $dia = "Qui";
						break;
		case "Friday"     : $dia = "Sex";
						break;
		case "Saturday"	    : $dia = "Sab";
						break;
    }	
	return $dia.', '.$data["mday"].' de '.$mes.' de '.$data["year"];
}	

function diferencaDatas($data1, $data2="",$tipo=""){ 
	if($data2==""){ 
		$data2 = date("Y-m-d H:i"); 
	} 
	if($tipo==""){ 
		$tipo = "d"; 
	} 
	for($i=1;$i<=2;$i++){ 
		${"ano".$i} = substr(${"data".$i},0,4); //dia
		${"mes".$i} = substr(${"data".$i},5,2); //mes
		${"dia".$i} = substr(${"data".$i},8,2); //ano
		${"horas".$i} = substr(${"data".$i},11,2); //horas
		${"minutos".$i} = substr(${"data".$i},14,2); //minutos
	} 
	$segundos = mktime($horas2,$minutos2,0,$mes2,$dia2,$ano2)-mktime($horas1,$minutos1,0,$mes1,$dia1,$ano1); 
	switch($tipo){ 
		case "m": $difere = $segundos/60;    break; 
		case "H": $difere = $segundos/3600;    break; 
		case "h": $difere = round($segundos/3600);    break; 
		case "D": $difere = $segundos/86400;    break; 
		case "d": $difere = round($segundos/86400);    break; 
  	} 
	return $difere; 
}

function enviarEmail($para,$de,$nome_de,$assunto,$msg){
	$mensagem = new email();
	$mensagem->set_format_mime();
	$mensagem->set_priority(1);
	$mensagem->subject = $assunto;
	$mensagem->add_to($para);
	$mensagem->add_from($de,$nome_de);
	$mensagem->add_text_mime('
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Formul�rio de contato</title>
<link href="http://www.duasbarras.com.br/include/duasbarras.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.texto_menor_10 {
	font-family: Trebuchet MS;
	font-size: 10px;
	color: #333333;
	text-decoration: none
}
.texto_menor_10 a:{
	font-family: Trebuchet MS;
	font-size: 10px;
	color: #333333;
	text-decoration: none	
}
.texto_menor_10 a: link{
	font-family: Trebuchet MS;
	font-size: 10px;
	color: #333333;
	text-decoration: none	
}
.texto_menor_10 a: hover{
	font-family: Trebuchet MS;
	font-size: 10px;
	color: #333333;
	text-decoration: none	
}
.texto_menor_10 a: visited{
	font-family: Trebuchet MS;
	font-size: 10px;
	color: #333333;
	text-decoration: none	
}
.texto_padrao {
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #333333;
	text-decoration: none
}
.texto_padrao a:{
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #333333;
	text-decoration: none	
}
.texto_padrao a: link{
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #333333;
	text-decoration: none	
}
.texto_padrao a: hover{
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #333333;
	text-decoration: none	
}
.texto_padrao a: visited{
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #333333;
	text-decoration: none	
}
.texto_padrao_sublinhado {
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #333333;
	text-decoration: underline
}
.texto_cinza {
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #EFEFEF;
}
.texto_cinza_escuro {
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #999999;
	text-decoration: none
}
.texto_cinza_escuro a:{
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #999999;
	text-decoration: none	
}
.texto_cinza_escuro a: link{
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #999999;
	text-decoration: none	
}
.texto_cinza_escuro a: hover{
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #999999;
	text-decoration: none	
}
.texto_cinza_escuro a: visited{
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #999999;
	text-decoration: none	
}
.texto_branco {
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #ffffff;
}
.texto_branco_negrito {
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #ffffff;
	font-weight: bold
}
.texto_azul {
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #006699;
	text-decoration: underline;
}
.texto_azul_gr {
	font-family: Trebuchet MS;
	font-size: 14px;
	color: #006699;
}
.texto_azul_gr_negrito_sublinhado {
	font-family: Trebuchet MS;
	font-size: 14px;
	color: #006699;
	text-decoration: underline;
	font-weight: bold
}
.texto_preto_negrito {
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #000000;
	font-weight: bold
}
-->
</style>
</head>

<body>
<table width="600" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
  <tr>
    <td bordercolor="#FFFFFF" bgcolor="#FFFFFF"><table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="http://www.duasbarras.com.br/img/topo_email.gif" width="600" height="98" /></td>
      </tr>
      <tr>
        <td bgcolor="#eeeeee"><div align="right">
          <table width="580" height="20" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td><div align="right">Nova Friburgo, '. data().' </div></td>
            </tr>
          </table>
          </div></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><table width="600" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td>'.$msg.'</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><img src="http://www.duasbarras.com.br/img/rodape.gif" width="600" height="40" border="0" usemap="#Map" /></td>
      </tr>
    </table></td>
  </tr>
</table>
<p align="center">&nbsp;</p>
</body>
</html>

');
	if($mensagem->send()) return true;
	return false;
}

function cadastrarEmail($email){
	$data_operacao = converteDataUsuario(date('d/m/Y'));
	$hora_operacao = date('H:i:s');	
	$cliente_maladireta = new cliente($email);
	$banco = new BD();
	$consulta = "select * from emails where EMAIL='$email' and ATIVO='S'";
	$resultado = $banco->pesquisarBD($consulta);
	$dados = $banco->mostra_registros($resultado);
	if($email == $dados["EMAIL"]){
   		return false; // ja cadastrado
   	}
	$consulta2 = "select * from emails where EMAIL='$email' and ATIVO='N'";
	$resultado2 = $banco->pesquisarBD($consulta2);
	$dados2 = $banco->mostra_registros($resultado2);
	if($email == $dados2["EMAIL"]){
   		$insere = "update emails set ATIVO='S',DATA_OPERACAO='$data_operacao',HORA_OPERACAO='$hora_operacao' where EMAIL='$email'";
   	}
	else 	$insere = "insert into emails(EMAIL,DATA_OPERACAO,HORA_OPERACAO)values('$email','$data_operacao','$hora_operacao')";	
	if(($banco->pesquisarBD($insere))&&($cliente_maladireta->setMaladireta())){
		return true;
	}
	return false;
}

function removerEmail($email){
	$data_operacao = converteDataUsuario(date('d/m/Y'));
	$hora_operacao = date('H:i:s');
	$banco = new BD();
	$consulta = "update emails set ATIVO='N',DATA_OPERACAO='$data_operacao',HORA_OPERACAO='$hora_operacao' where EMAIL='$email'";
	if($banco->pesquisarBD($consulta))	return true;
	return false;
}

function atualizarEmail($email_antigo,$email_novo){
	$banco = new BD();
	$consulta = "select * from emails where EMAIL='$email_novo'";
	$resultado = $banco->pesquisarBD($consulta);
	$dados = $banco->mostra_registros($resultado);
	if(!$dados){
		$atualiza = "update emails set EMAIL='$email_novo' where EMAIL='$email_antigo'";
		if($banco->pesquisarBD($atualiza)) return true;
		return false;
	}
	else if(removerEmail($email_antigo)) return true;
	return false;
}

function exibirValor($valor){
	return SIMBOLO_MOEDA.number_format($valor,2,',','.');
}

function diminuiArray($array,$valor){
	if((!$array)||(!$valor)) return false;
	$chave = array_search($valor,$array);
	$parte1 = array_slice($array,$chave+1);
	$parte2 = array_slice($array,0,$chave);
	return array_merge($parte1,$parte2);
}

function campoAbreviado($campo,$qtdCarac='10'){
	if(strlen($campo)>$qtdCarac)	return substr($campo,0,($qtdCarac-3))."...";
	return $campo;
}

function cadastrarIndicacao($nome_amigo, $email_amigo, $nome_envia, $email_envia){
	$banco = new BD();
	$data_operacao = converteDataUsuario(date('d/m/Y'));
	$hora_operacao = date('H:i:s');
	$insere = "insert into indicacoes (NOME_AMIGO, EMAIL_AMIGO, NOME_ENVIA, EMAIL_ENVIA, DATA_OPERACAO, HORA_OPERACAO, COD_ACESSO)values('$nome_amigo', '$email_amigo', '$nome_envia', '$email_envia', '$data_operacao', '$hora_operacao', '$cod_acesso')";
	if($banco->pesquisarBD($insere)){
		return $insere; // indicacao foi cadastrada
	}
	return false; // erro no cadastro da indicacao
}
function textoDinamico($pagina){
	$banco = new BD();
	$consulta = "select * from layout where pagina='$pagina'";
	$resultado = $banco->pesquisarBD($consulta);
	$dados = $banco->mostra_registros($resultado);
	if($pagina == $dados["pagina"]){
		return $dados["texto"];
	}
}

function cadastrarNewsletter($nome,$email,$telefone,$cidade,$estado,$pais){
	$banco = new BD();
	$consulta = "select * from newsletter where email='$email'";
	$resultado = $banco->pesquisarBD($consulta);
	$dados = $banco->mostra_registros($resultado);
	if($dados["email"]){
		return false;
	}
	$data_operacao = converteDataUsuario(date('d/m/Y'));
	$hora_operacao = date('H:i:s');
	$insere = "insert into newsletter (nome, email, telefone, cidade, estado, pais, data_operacao, hora_operacao)values('$nome', '$email', '$telefone', '$cidade', '$estado', '$pais', '$data_operacao', '$hora_operacao')";
	if($banco->pesquisarBD($insere)){
		return true; // email foi cadastrado
	}
	return false; // erro no cadastro do email
}
?>