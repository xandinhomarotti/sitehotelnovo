<!--

	+------------------------------------------------------------------+
	|  FriServ.com - Solu��es de Alto Impacto (www.friserv.com)        |
	|  ASMNet Sistemas (www.asmnet.com.br)                             |
	| ---------------------------------------------------------------- |
	|  M�dulo Loja Virtual                                             |
	| ---------------------------------------------------------------- |
	|  #Autor: Carlos Eduardo Quima Werner (contato@friserv.com)       |
	|  #Autor: Ruda R. M. dos Santos (ruda@asmnet.com.br)              |	
	|  #Autor: Thiago Gomes Menezes (thiago@friserv.com)               |	
	|  In�cio do Projeto: 13 de novembro de 2006 / 15:00h              |
	|  Cliente: Altarede - Loja Virtual                                |
	+------------------------------------------------------------------+
	
-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="author" content="FriServ.com - www.friserv.com" />
<meta name="title" content="FriServ.com - Solu��es de Alto Impacto - Nova Friburgo - RJ" />
<meta name="description" content="WebSite da Altarede" />
<meta name="keywords" content="altarede, internet a cabo, equipamentos para informatica" />
<meta name="title" content="Altarede" /> 
<meta name="url" content="http://www.altarede.com" /> 
<meta name="category" content="business, internet a cabo, equipamentos para informatica" /> 
<meta name="author" content="FriServ.com - www.friserv.com | ASMNet - www.asmnet.com.br" />
<meta name="robots" content="all" />
<meta name="distribution" content="global" />
<meta name="language" content="pt-br" />
<link rev=made href="mailto: contato@altarede.com.br">
<meta name="copyright" CONTENT="FriServ.com">
<title>Confirma&ccedil;&atilde;o de cadastro</title>
<link href="../include/altarede.css" rel="stylesheet" type="text/css" />
<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="../Scripts/funcoes.js" type="text/javascript"></script>
<style type="text/css">
<!--
.texto_menor_10 {
	font-family: Trebuchet MS;
	font-size: 10px;
	color: #333333;
	text-decoration: none
}
.texto_menor_10 a:{
	font-family: Trebuchet MS;
	font-size: 10px;
	color: #333333;
	text-decoration: none	
}
.texto_menor_10 a: link{
	font-family: Trebuchet MS;
	font-size: 10px;
	color: #333333;
	text-decoration: none	
}
.texto_menor_10 a: hover{
	font-family: Trebuchet MS;
	font-size: 10px;
	color: #333333;
	text-decoration: none	
}
.texto_menor_10 a: visited{
	font-family: Trebuchet MS;
	font-size: 10px;
	color: #333333;
	text-decoration: none	
}
.texto_padrao {
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #333333;
	text-decoration: none
}
.texto_padrao a:{
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #333333;
	text-decoration: none	
}
.texto_padrao a: link{
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #333333;
	text-decoration: none	
}
.texto_padrao a: hover{
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #333333;
	text-decoration: none	
}
.texto_padrao a: visited{
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #333333;
	text-decoration: none	
}
.texto_padrao_sublinhado {
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #333333;
	text-decoration: underline
}
.texto_cinza {
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #EFEFEF;
}
.texto_cinza_escuro {
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #999999;
	text-decoration: none
}
.texto_cinza_escuro a:{
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #999999;
	text-decoration: none	
}
.texto_cinza_escuro a: link{
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #999999;
	text-decoration: none	
}
.texto_cinza_escuro a: hover{
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #999999;
	text-decoration: none	
}
.texto_cinza_escuro a: visited{
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #999999;
	text-decoration: none	
}
.texto_branco {
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #ffffff;
}
.texto_branco_negrito {
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #ffffff;
	font-weight: bold
}
.texto_azul {
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #006699;
	text-decoration: underline;
}
.texto_azul_gr {
	font-family: Trebuchet MS;
	font-size: 14px;
	color: #006699;
}
.texto_azul_gr_negrito_sublinhado {
	font-family: Trebuchet MS;
	font-size: 14px;
	color: #006699;
	text-decoration: underline;
	font-weight: bold
}
.texto_preto_negrito {
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #000000;
	font-weight: bold
}
-->
</style>
</head>

<body>
<p align="center"><br />
  N&atilde;o   consegue visualizar a mensagem? Tenha acesso &agrave;s nossas superofertas   acessando:<br />
  <a href="http://www.nomedosite.com.br" target="_blank" class="texto_padrao">http://www.nomedosite.com.br</a></p>
<table width="600" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
  <tr>
    <td bordercolor="#FFFFFF" bgcolor="#FFFFFF"><table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="img/topo_email.gif" alt="Confirma&ccedil;&atilde;o de cadastro" width="600" height="98" /></td>
      </tr>
      <tr>
        <td bgcolor="#eeeeee"><div align="right">
          <table width="580" height="20" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td><div align="right">Nova Friburgo, 88 de Nonononono de 2900 (data do envio) </div></td>
            </tr>
          </table>
          </div></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><table width="580" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td><table width="450" height="60" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
                <tr>
                  <td bordercolor="#FFFFFF" bgcolor="#FFFFFF"><div align="center"><span class="style1">Para </span>confirmar seu cadastro acesse o link abaixo:<br />
                    <a href="http://www.nomedosite.com.br/paginadeconfirmacao.php" target="_blank" class="texto_padrao">http://www.nomedosite.com.br/paginadeconfirmacao.php</a></div></td>
                </tr>
              </table>
              <br />
              <table width="550" border="0" align="center" cellpadding="0" cellspacing="0">

              <tr>
                <td colspan="2" background="img/linha.gif">&nbsp;</td>
                </tr>
              <tr>
                <td colspan="2">Confira os dados cadastrados em nossa base de dados: </td>
              </tr>
              <tr>
                <td colspan="2" background="img/linha.gif">&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td width="150">&nbsp;</td>
                <td><img src="img/tit_dados_preliminares.gif" alt="dados cadastrados" width="165" height="18" /></td>
              </tr>
              <tr>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
              </tr>
              <tr>
                <td width="150" height="25">Nome | Raz&atilde;o social: </td>
                <td height="25">Nononononononononononononononono</td>
              </tr>
              <tr>
                <td width="150"><img src="../img/spacer.gif" width="5" height="5" /></td>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
              </tr>
              <tr>
                <td width="150" height="25">CPF | CNPJ:</td>
                <td height="25">Nononononononononononononononono</td>
              </tr>
              <tr>
                <td width="150"><img src="../img/spacer.gif" width="5" height="5" /></td>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
              </tr>
              <tr>
                <td width="150" height="25">RG | IE: </td>
                <td height="25">Nononononononononononononononono</td>
              </tr>
              <tr>
                <td width="150"><img src="../img/spacer.gif" width="5" height="5" /></td>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
              </tr>
              <tr>
                <td width="150" height="25">Sexo: </td>
                <td height="25">Nononononononononononononononono</td>
              </tr>
              <tr>
                <td width="150"><img src="../img/spacer.gif" width="5" height="5" /></td>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
              </tr>
              <tr>
                <td width="150" height="25">Nome para contato: </td>
                <td height="25">Nononononononononononononononono</td>
              </tr>
              <tr>
                <td width="150"><img src="../img/spacer.gif" width="5" height="5" /></td>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
              </tr>
              <tr>
                <td width="150" height="25">Data de Nascimento:</td>
                <td height="25">Nononononononononononononononono</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td colspan="2" background="img/linha.gif">&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><img src="img/tit_dados_endereco.gif" alt="dados cadastrados" width="165" height="19" /></td>
              </tr>
              <tr>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
              </tr>
              <tr>
                <td width="150" height="25">Tipo de endere&ccedil;o:</td>
                <td height="25">Nononononononononononononononono</td>
              </tr>
              <tr>
                <td width="150"><img src="../img/spacer.gif" width="5" height="5" /></td>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
              </tr>
              <tr>
                <td width="150" height="25"><? if($cep->logradouro!=''){echo "$cep->logradouro";}else {echo "Logradouro";}?>
:</td>
                <td height="25">Nononononononononononononononono</td>
              </tr>
              <tr>
                <td width="150"><img src="../img/spacer.gif" width="5" height="5" /></td>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
              </tr>
              <tr>
                <td width="150" height="25">N&uacute;mero:</td>
                <td height="25">Nononononononononononononononono</td>
              </tr>
              <tr>
                <td width="150"><img src="../img/spacer.gif" width="5" height="5" /></td>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
              </tr>
              <tr>
                <td width="150" height="25">Complemento:</td>
                <td height="25">Nononononononononononononononono</td>
              </tr>
              <tr>
                <td width="150"><img src="../img/spacer.gif" width="5" height="5" /></td>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
              </tr>
              <tr>
                <td width="150" height="25">Bairro:</td>
                <td height="25">Nononononononononononononononono</td>
              </tr>
              <tr>
                <td width="150"><img src="../img/spacer.gif" width="5" height="5" /></td>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
              </tr>
              <tr>
                <td width="150" height="25">Cidade:</td>
                <td height="25">Nononononononononononononononono</td>
              </tr>
              <tr>
                <td width="150"><img src="../img/spacer.gif" width="5" height="5" /></td>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
              </tr>
              <tr>
                <td width="150" height="25">Estado:</td>
                <td height="25">Nononononononononononononononono</td>
              </tr>
              <tr>
                <td width="150"><img src="../img/spacer.gif" width="5" height="5" /></td>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
              </tr>
              <tr>
                <td width="150" height="25">CEP:</td>
                <td height="25">Nononononononononononononononono</td>
              </tr>
              <tr>
                <td width="150"><img src="../img/spacer.gif" width="5" height="5" /></td>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
              </tr>
              <tr>
                <td width="150" height="25">Telefone:</td>
                <td height="25">Nononononononononononononononono</td>
              </tr>
              <tr>
                <td width="150"><img src="../img/spacer.gif" width="5" height="5" /></td>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
              </tr>
              <tr>
                <td width="150" height="25">Celular | Fax: </td>
                <td height="25">Nononononononononononononononono</td>
              </tr>
              <tr>
                <td width="150"><img src="../img/spacer.gif" width="5" height="5" /></td>
                <td><img src="../img/spacer.gif" width="5" height="5" /></td>
              </tr>
              <tr>
                <td height="25">Receber newsletter:</td>
                <td height="25">Nononononononononononononononono</td>
              </tr>
              <tr>
                <td height="25" colspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td height="25" colspan="2" background="img/linha.gif">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="2">&nbsp;</td>
                </tr>
              <tr>
                <td height="25" colspan="2"><div align="center" class="texto_padrao"><a href="http://www.friserv.com" target="_blank" class="texto_padrao">Departamento</a> | <a href="http://www.friserv.com" target="_blank" class="texto_padrao">Departamento</a><a href="http://www.friserv.com" target="_blank"></a><a href="http://umail.americanas.com.br/ta.aspx?p=1212437&amp;c=MjI0ODcw&amp;e=thiago@friserv.com&amp;l=OTczNDM=&amp;U=aHR0cDovL3d3dy5hbWVyaWNhbmFzLmNvbS5ici9jYXQvNTg5L0RWRFN0b3JlP2k9MSZvcG49VFdPUFlT" target="_blank"></a> | <a href="http://www.friserv.com" target="_blank" class="texto_padrao">Departamento</a><a href="http://www.friserv.com" target="_blank"></a><a href="http://umail.americanas.com.br/ta.aspx?p=1212437&amp;c=MjI0ODcw&amp;e=thiago@friserv.com&amp;l=OTczNDQ=&amp;U=aHR0cDovL3d3dy5hbWVyaWNhbmFzLmNvbS5ici9jYXQvMTQ3Mi9Cb29rU3RvcmU/aT0xJm9wbj1UV09QWVM=" target="_blank"></a> | <a href="http://www.friserv.com" target="_blank" class="texto_padrao">Departamento</a><a href="http://www.friserv.com" target="_blank"></a><a href="http://umail.americanas.com.br/ta.aspx?p=1212437&amp;c=MjI0ODcw&amp;e=thiago@friserv.com&amp;l=OTczNDU=&amp;U=aHR0cDovL3d3dy5hbWVyaWNhbmFzLmNvbS5ici9jYXQvMTg1OC9DYXRhbG9nbz9pPTEmb3BuPVRXT1BZUw==" target="_blank"></a> | <a href="http://www.friserv.com" target="_blank" class="texto_padrao">Departamento</a><a href="http://www.friserv.com" target="_blank"></a><a href="http://umail.americanas.com.br/ta.aspx?p=1212437&amp;c=MjI0ODcw&amp;e=thiago@friserv.com&amp;l=OTczNDY=&amp;U=aHR0cDovL3d3dy5hbWVyaWNhbmFzLmNvbS5ici9jYXQvNTkwL2VhY29tP2k9MSZvcG49VFdPUFlT" target="_blank"></a><br />
                    <a href="http://www.friserv.com" target="_blank" class="texto_padrao">Departamento</a><a href="http://www.friserv.com" target="_blank"></a><a href="http://umail.americanas.com.br/ta.aspx?p=1212437&amp;c=MjI0ODcw&amp;e=thiago@friserv.com&amp;l=OTczNDc=&amp;U=aHR0cDovL3d3dy5hbWVyaWNhbmFzLmNvbS5ici9jYXQvNjg3Mi9lYWNvbT9pPTEmb3BuPVRXT1BZUw==" target="_blank"></a> | <a href="http://www.friserv.com" target="_blank" class="texto_padrao">Departamento</a><a href="http://www.friserv.com" target="_blank"></a> | <a href="http://www.friserv.com" target="_blank" class="texto_padrao">Departamento</a><a href="http://www.friserv.com" target="_blank"></a></div></td>
              </tr>
              <tr>
                <td height="25" colspan="2">&nbsp;</td>
                </tr>
            </table>
              </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><img src="img/rodape.gif" alt="desenvolvido por FriServ.com | ASMNet" width="600" height="40" border="0" usemap="#Map" /></td>
      </tr>
    </table></td>
  </tr>
</table>
<p align="center">&nbsp;</p>

<map name="Map" id="Map"><area shape="rect" coords="404,-9,496,59" href="http://www.asmnet.com.br" target="_blank" />
<area shape="rect" coords="497,-19,616,46" href="http://www.friserv.com" target="_blank" />
</map></body>
</html>
