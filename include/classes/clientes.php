<?php
include_once "include/funcoesUteis.php";
include_once "include/classes/BD.php";
//_________________________________________________________________________________________________
// definindo as tabelas relacionadas a clientes
//_________________________________________________________________________________________________
define('TABELA_CLIENTES', 'clientes');
define('TABELA_CLIENTES_PJ','clientes_pj');
define('TABELA_CLIENTES_PF','clientes_pf');
define('TABELA_PEDIDOS','pedidos');

class cliente{

	var $id_cliente;
	var $tel1_cliente;
	var $tel2_cliente;
	var $email_cliente;
	var $senha_cliente;
	var $dica_senha_cliente;
	var $apelido_cliente;
	var $maladireta_cliente;
	var $tipo_endereco_cliente;
	var $logradouro_endereco_cliente;
	var $rua_endereco_cliente;
	var $numero_endereco_cliente;
	var $complemento_endereco_cliente;
	var $referencia_endereco_cliente;
	var $cep_endereco_cliente;
	var $bairro_endereco_cliente;
	var $cidade_endereco_cliente;
	var $estado_endereco_cliente;
	var $tipo_cliente;
	var $finalizado = 'N';
	var $data_cadastro_cliente;
	var $data_confirmacao_cliente;	
	var $confirmacaoCadastro = "email_confirmacao.php";
	var $banco;
	var $novaChave;
	var $expira_cadastro = 30;
	var $envia_lembrete = 25;
	var $lembrete_cadastro;
	var $segundavia_confirmacao;
	var $sql;
	
	function cliente($email='',$senha=''){
		$this->banco = new BD;
		$consulta = "select * from ".TABELA_CLIENTES." where EMAIL='$email' and
		SENHA=password('$senha')";
		$resultado = $this->banco->pesquisarBD($consulta);
		$dados = $this->banco->mostra_registros($resultado);
		if($dados){
			$this->id_cliente = $dados["COD_CLIENTE"];
			$this->tel1_cliente = $dados["TELEFONE_RESIDENCIAL"];
			$this->tel2_cliente = $dados["TELEFONE_CELULAR"];
			$this->email_cliente = $dados["EMAIL"];
			$this->senha_cliente = $dados["SENHA"];
			$this->dica_senha_cliente = $dados["DICA_SENHA"];			
			$this->apelido_cliente = $dados["APELIDO"];
			$this->maladireta_cliente = $dados["MALADIRETA"];
			$this->tipo_endereco_cliente = $dados["TIPO_ENDERECO"];
			$this->logradouro_endereco_cliente = $dados["LOGRADOURO"];
			$this->rua_endereco_cliente = $dados["ENDERECO"];
			$this->numero_endereco_cliente = $dados["NUMERO"];
			$this->complemento_endereco_cliente = $dados["COMPLEMENTO"];
			$this->referencia_endereco_cliente = $dados["REFERENCIA"];
			$this->cep_endereco_cliente = $dados["CEP"];
			$this->bairro_endereco_cliente = $dados["BAIRRO"];
			$this->cidade_endereco_cliente = $dados["CIDADE"];
			$this->estado_endereco_cliente = $dados["ESTADO"];
			$this->tipo_cliente = $dados["TIPO_CLIENTE"];
			$this->finalizado = $dados["FINALIZADO"];
			$this->data_cadastro_cliente = $dados["DATA_CADASTRO"];
			$this->data_confirmacao_cliente = $dados["DATA_CONFIRMACAO"];			
			return true;
		}
		$this->email_cliente = $email;
		$this->senha_cliente = $senha;
		return false;
	}
	
	function lembrarSenha(){
		$banco = new BD();
		$consulta = "select * from ".TABELA_CLIENTES." where EMAIL='$this->email_cliente'";
		$resultado = $banco->pesquisarBD($consulta);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$this->tipo_cliente=$dados["TIPO_CLIENTE"];
			return  $dados["DICA_SENHA"];
		}
		return false;
	}
		
	function novaSenha(){
		$banco = new BD();
		$consulta = "update ".TABELA_CLIENTES." set DICA_SENHA='$this->dica_senha_cliente', SENHA=password('$this->senha_cliente') where EMAIL='$this->email_cliente'";
		if($resultado = $banco->pesquisarBD($consulta))	return true;
			return false;
	}
		
	function verificaCadastro($email){
		$verifica = "select * from ".TABELA_CLIENTES." where EMAIL = '$email' and FINALIZADO = 'S'";
		$resultado = $this->banco->pesquisarBD($verifica);
		$dados = $this->banco->mostra_registros($resultado);
		$verifica2 = "select * from ".TABELA_CLIENTES." where EMAIL = '$email' and FINALIZADO = 'N'";
		$resultado2 = $this->banco->pesquisarBD($verifica2);
		$dados2 = $this->banco->mostra_registros($resultado2);		
		if($dados){
			return 1; // o email existe e est� em uso
		}
		if($dados2){
			return 2; // o email existe mas n�o est� em uso
		}		
		return false;
	}

	function cadastrarCliente()
	{
		$data_temp=date('d/m/Y');
		$this->data_cadastro_cliente = converteDataUsuario($data_temp);		
		$insere = "insert into ".TABELA_CLIENTES." 
		(TELEFONE_RESIDENCIAL, TELEFONE_CELULAR, EMAIL, 
		SENHA, DICA_SENHA, APELIDO, MALADIRETA, 
		TIPO_ENDERECO, LOGRADOURO, ENDERECO, 
		NUMERO, COMPLEMENTO, 
		REFERENCIA, CEP, 
		BAIRRO, CIDADE, 
		ESTADO, TIPO_CLIENTE, FINALIZADO, DATA_CADASTRO, EMAIL_SEGUNDAVIA, EMAIL_LEMBRETE)
		values
		('$this->tel1_cliente', '$this->tel2_cliente', '$this->email_cliente', 
		password('$this->senha_cliente'), '$this->dica_senha_cliente', '$this->apelido_cliente',
		'$this->maladireta_cliente', '$this->tipo_endereco_cliente', '$this->logradouro_endereco_cliente', '$this->rua_endereco_cliente', 
		'$this->numero_endereco_cliente', '$this->complemento_endereco_cliente', 
		'$this->referencia_endereco_cliente', '$this->cep_endereco_cliente', 
		'$this->bairro_endereco_cliente', '$this->cidade_endereco_cliente', 
		'$this->estado_endereco_cliente', '$this->tipo_cliente', '$this->finalizado', '$this->data_cadastro_cliente', '$this->segundavia_confirmacao', '$this->lembrete_cadastro')";
		if($this->maladireta_cliente=='S') cadastrarEmail($this->email_cliente);
		if($this->banco->pesquisarBD($insere)){
			$this->novaChave = novoCodigo(TABELA_CLIENTES,"COD_CLIENTE");
			return true; // cliente foi cadastrado
		}
		return false; // erro no cadastro do cliente
	}

	function confirmaCadastro()
	{
		$data_temp=date('d/m/Y');
		$data = converteDataUsuario($data_temp);
		if($this->finalizado=='S'){
			return 1;  // o cliente ja confimou o cadastro anteriormente
		}
		if((diferencaDatas($this->data_cadastro_cliente,date('Y-m-d'),"d"))>$this->expira_cadastro){
			return 2;  // a data expirou
		}
		$altera = "update ".TABELA_CLIENTES." set FINALIZADO='S', DATA_CONFIRMACAO='".date('Y-m-d')."' where EMAIL = '$this->email_cliente'";
		if($this->banco->pesquisarBD($altera)){
			return 3; // a confirmacao de cadastro do cliente foi feita
		}
		return 4; // erro na confirmacao de cadastro do cliente
	}

	function alterarSenhaCliente()
	{
		$altera = "update ".TABELA_CLIENTES." set SENHA=password('$this->senha_cliente'),  	
		DICA_SENHA='$this->dica_senha_cliente' where EMAIL = '$this->email_cliente'";
		if($this->banco->pesquisarBD($altera)){
			return true; // cliente foi alterado
		}
		return false; // erro na altera��o do cliente
	}

	function alterarEmailCliente($email_novo)
	{
		$banco = new BD();
		$consulta = "select * from ".TABELA_CLIENTES." where EMAIL='$email_novo'";
		$resultado = $banco->pesquisarBD($consulta);
		$dados = $banco->mostra_registros($resultado);
		if(!$dados){	
			$altera = "update ".TABELA_CLIENTES." set EMAIL='$email_novo' where EMAIL = '$this->email_cliente'";
			if($this->banco->pesquisarBD($altera)){
			$this->email_cliente = $email_novo;
			return true; // cliente foi alterado
			}
			return false; // erro na altera��o do cliente
		}
		return false; // email j� cadastrado
	}

	function alterarCliente()
	{
		$banco = new BD();
		$altera = "update ".TABELA_CLIENTES." set TELEFONE_RESIDENCIAL='$this->tel1_cliente',
		TELEFONE_CELULAR='$this->tel2_cliente',
		APELIDO='$this->apelido_cliente',
		MALADIRETA='$this->maladireta_cliente', 
		TIPO_ENDERECO='$this->tipo_endereco_cliente',
		LOGRADOURO='$this->logradouro_endereco_cliente',
		ENDERECO='$this->rua_endereco_cliente', 
		NUMERO='$this->numero_endereco_cliente', 	
		COMPLEMENTO='$this->complemento_endereco_cliente', 
		REFERENCIA='$this->referencia_endereco_cliente', 	
		CEP='$this->cep_endereco_cliente', 
		BAIRRO='$this->bairro_endereco_cliente',
		CIDADE='$this->cidade_endereco_cliente', 
		ESTADO='$this->estado_endereco_cliente',
		TIPO_CLIENTE='$this->tipo_cliente' where EMAIL = '$this->email_cliente'";
		$this->sql = $altera;
		if($banco->pesquisarBD($altera)){
			return true; // cliente foi alterado
		}
		return false; // erro na altera��o do cliente
	}
	
	function excluirCliente(){	
		$excluir = "delete from ".TABELA_CLIENTES." where EMAIL='$this->email-cliente'";
		if($this->banco->pesquisarBD($excluir)){
			return true; // cliente foi excluido
		}
		return false; // erro na exclus�o do cliente
	}
	
	function segundaviaEmail(){
		$consulta = "select EMAIL_SEGUNDAVIA from ".TABELA_CLIENTES." where EMAIL='$this->email_cliente'";
		$resultado = $this->banco->pesquisarBD($consulta);
		$dados = $this->banco->mostra_registros($resultado);
		if($dados) return  $dados["EMAIL_SEGUNDAVIA"];
		return false;
	}
	
	function validarSenha($senha){
		$banco = new BD();
		$consulta = "select * from ".TABELA_CLIENTES." where SENHA=password('$senha')";
		$resultado = $banco->pesquisarBD($consulta);
		$dados = $banco->mostra_registros($resultado);
		if($dados) return  true;
		return false;
	}
	
	function setMaladireta(){
		$banco = new BD();
		$consulta = "select * from ".TABELA_CLIENTES." where EMAIL='$this->email_cliente' and MALADIRETA='N'";
		$resultado = $banco->pesquisarBD($consulta);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$altera = "update ".TABELA_CLIENTES." set MALADIRETA='S'";
			if($banco->pesquisarBD($altera)) return true;
			return false;
		}
		return true;
	}
	
	function consultarPedidos(){
		$banco = new BD();
		$consulta = "select * from ".TABELA_PEDIDOS." WHERE COD_CLIENTE = '$this->id_cliente'";
		$this->sql= $consulta;
		$retorno = array();
		$cont=0;
		$resultado = $banco->pesquisarBD($consulta);
		while($dados = $banco->mostra_registros($resultado)){
			$retorno[$cont] = $dados["COD_PEDIDO"];
			$cont++;
		}
		return $retorno;
	}
}
/*---------------------------------------------------------------------------------------------------
Classe cliente pessoa juridica derivada da classe clientes
---------------------------------------------------------------------------------------------------*/
class cliente_pj extends cliente{
	
	var $id_cliente_pj;
	var $razao_social_cliente_pj;
	var $nome_contato_cliente_pj;
	var $cnpj_cliente_pj;
	var $ie_cliente_pj;

	function cliente_pj($email='',$senha='')
	{
		parent::cliente($email,$senha);
		$consulta = "select * from ".TABELA_CLIENTES_PJ." where COD_CLIENTE_PJ='$this->id_cliente'";
		$resultado = $this->banco->pesquisarBD($consulta);
		if($dados = $this->banco->mostra_registros($resultado)){
			$this->id_cliente_pj = $dados["COD_CLIENTE_PJ"];
			$this->razao_social_cliente_pj = $dados["NOME"];
			$this->nome_contato_cliente_pj = $dados["RESPONSAVEL"];
			$this->cnpj_cliente_pj = $dados["CNPJ"];
			$this->ie_cliente_pj = $dados["IE"];
			return true;
		}
		return false;	
	}
	
	function cadastrarCliente()
	{
		parent::cadastrarCliente();
		$insere = "insert into ".TABELA_CLIENTES_PJ." 
		(COD_CLIENTE_PJ, NOME, RESPONSAVEL, 
		CNPJ, IE)
		values
		('$this->novaChave', '$this->razao_social_cliente_pj', '$this->nome_contato_cliente_pj', 
		'$this->cnpj_cliente_pj', '$this->ie_cliente_pj')";
		if($this->banco->pesquisarBD($insere)){
			return true; // cliente juridico foi cadastrado
		}
		return false; // erro na altera��o do cliente juridico
	}
	
	function alterarCliente(){
		$banco = new BD();
		$alterou = false;
		if(parent::alterarCliente()) $alterou = true;
		$altera = "update ".TABELA_CLIENTES_PJ." set 
		NOME='$this->razao_social_cliente_pj', 
		RESPONSAVEL='$this->nome_contato_cliente_pj', 
		CNPJ='$this->cnpj_cliente_pj', IE='$this->ie_cliente_pj'
		where COD_CLIENTE_PJ = '$this->id_cliente_pj'";
		if(($banco->pesquisarBD($altera))&&($alterou)){
			return true; // cliente juridico foi alterado
		}
		return false; // erro na altera��o do cliente juridico
	}
	
	function excluirCliente(){	
		parent::excluirCliente();
		$excluir = "delete from ".TABELA_CLIENTES_PJ." where COD_CLIENTE_PJ = '$this->id_cliente_pj'";
		if($this->banco->pesquisarBD($altera)){
			return true; // cliente juridico foi excluido
		}
		return false; // erro na exclus�o do cliente juridico
	}
	
	function setApelidoJ($apelido=''){
		if($apelido==''){
			$nome = explode (" ", $this->nome_contato_cliente_pj);//tira o espa�o
			$this->apelido_cliente = $nome[0];
		}
		else $this->apelido_cliente = $apelido;
	}	  
}
/*---------------------------------------------------------------------------------------------------
Classe cliente pessoa fisica derivada da classe clientes
---------------------------------------------------------------------------------------------------*/
class cliente_pf extends cliente{
	
	var $id_cliente_pf;
	var $nome_cliente_pf;
	var $cpf_cliente_pf;
	var $rg_cliente_pf;
	var $sexo_cliente_pf;
	var $data_nascimento_cliente_pf;

	function getDataNascimento(){
		return converteDataBanco($this->data_nascimento_cliente_pf);
	}

	function cliente_pf($email='',$senha=''){
		parent::cliente($email,$senha);
		$consulta = "select * from ".TABELA_CLIENTES_PF." where COD_CLIENTE_PF='$this->id_cliente'";
		$resultado = $this->banco->pesquisarBD($consulta);
		if($dados = $this->banco->mostra_registros($resultado)){
			$this->id_cliente_pf = $dados["COD_CLIENTE_PF"];		
			$this->nome_cliente_pf = $dados["NOME"];
			$this->cpf_cliente_pf = $dados["CPF"];
			$this->rg_cliente_pf = $dados["IDENTIDADE"];
			$this->sexo_cliente_pf = $dados["SEXO"];
			$this->data_nascimento_cliente_pf = $dados["DATA_NASCIMENTO"];
			return false;
		}
		return false;
	}
			
	function setDataNascimento($data){
		$this->data_nascimento_cliente_pf = converteDataUsuario($data);
	}
	
	function cadastrarCliente()
	{
		parent::cadastrarCliente();
		$insere = "insert into ".TABELA_CLIENTES_PF." 
		(COD_CLIENTE_PF, NOME, CPF, 
		IDENTIDADE, SEXO, DATA_NASCIMENTO)
		values
		('$this->novaChave', '$this->nome_cliente_pf', '$this->cpf_cliente_pf', 
		'$this->rg_cliente_pf', '$this->sexo_cliente_pf', '$this->data_nascimento_cliente_pf')";
		if($this->banco->pesquisarBD($insere)){
			return true; // cliente fisico foi cadastrado
		}
		return false; // erro na altera��o do cliente fisico
	}
	
	function alterarCliente(){
		$alterou = false;
		if(parent::alterarCliente()) $alterou = true;
		$banco = new BD();
		$altera = "update ".TABELA_CLIENTES_PF." set 
		NOME='$this->nome_cliente_pf', 
		CPF='$this->cpf_cliente_pf', 
		IDENTIDADE='$this->rg_cliente_pf', SEXO='$this->sexo_cliente_pf', DATA_NASCIMENTO='$this->data_nascimento_cliente_pf'
		where COD_CLIENTE_PF = '$this->id_cliente_pf'";
		if(($banco->pesquisarBD($altera))&&($alterou)){
			return true; // cliente fisico foi alterado
		}
		return false; // erro na altera��o do cliente fisico
	}
	
	function excluirCliente(){	
		parent::excluirCliente();
		$excluir = "delete from ".TABELA_CLIENTES_PF." where COD_CLIENTE_PF = '$this->id_cliente_pf'";
		if($this->banco->pesquisarBD($altera)){
			return true; // cliente fisico foi excluido
		}
		return false; // erro na exclus�o do cliente fisico
	}
	
	function setApelidoF($apelido=''){
		if($apelido==''){
			$nome = explode (" ", $this->nome_cliente_pf);//tira o espa�o
			$this->apelido_cliente = $nome[0];
		}
		else $this->apelido_cliente = $apelido;
	}
	
	function novaSenha(){
		$banco = new BD();
		$consulta = "select * from ".TABELA_CLIENTES_PF." inner join ".TABELA_CLIENTES." on COD_CLIENTE=COD_CLIENTE_PF where EMAIL='$this->email_cliente'";
		$resultado = $banco->pesquisarBD($consulta);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			if(($this->cpf_cliente_pf==$dados["CPF"])&&($this->rg_cliente_pf==$dados["IDENTIDADE"])&&($this->data_nascimento_cliente_pf==$dados["DATA_NASCIMENTO"])){
				if(parent::novaSenha()) return 1; // cadastro nova senha
				return 2;  // erro no cadastro da nova senha
			}
		}
		return 3; // dados invalidos
	}
}
?>