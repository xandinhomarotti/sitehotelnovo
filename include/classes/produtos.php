<?php
include_once "include/funcoesUteis.php";
include_once "include/classes/BD.php";
//_________________________________________________________________________________________________
// definindo as tabelas relacionadas a produtos
//_________________________________________________________________________________________________
define('TABELA_DEPARTAMENTOS', 'departamentos');
define('TABELA_SUBDEPARTAMENTOS','subdepartamentos');
define('TABELA_TIPO_PRODUTOS','tipo_produtos');
define('TABELA_CAMPOS_PRODUTO_POR_TIPO_PRODUTO', 'campos_produto_por_tipo_produto');
define('TABELA_CAMPOS_PRODUTO','campos_produto');
define('TABELA_VALORES_CAMPOS_PRODUTOS','valores_campos_produtos');
define('TABELA_FABRICANTES', 'fabricantes');
define('TABELA_FOTOS_PRODUTO','fotos_produto');
define('TABELA_PRODUTOS','produtos');
define('TABELA_ESTOQUE','estoque');
define('TABELA_VALORES','valores_por_produto');

/*---------------------------------------------------------------------------------------------------
Classe fabricante
---------------------------------------------------------------------------------------------------*/
class fabricante{

	var $cod_fabricante;
	var $fabricante;
	var $descricao;	
	var $data_operacao;
	var $hora_operacao;
	var $exibicao;
	var $cod_acesso;
	var $visivel;
	
	function fabricante($cod_fabricante=''){
		$banco = new BD;
		$consulta = "select * from ".TABELA_FABRICANTES." where 
		COD_FABRICANTES='$cod_fabricante'";
		$resultado = $banco->pesquisarBD($consulta);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$this->cod_fabricante = $dados["COD_FABRICANTE"];
			$this->fabricante = $dados["FABRICANTE"];
			$this->descricao = $dados["DESCRICAO"];			
			$this->data_operacao = $dados["DATA_OPERACAO"];
			$this->hora_operacao = $dados["HORA_OPERACAO"];
			$this->cod_acesso = $dados["COD_ACESSO"];
			$this->visivel = $dados["VISIVEL"];			
		}
		$this->cod_fabricante = $cod_fabricante;
		$banco->fechar();
	}
	
	function verificaCadastroFabricante($fabricante){
		$banco = new BD;
		$verifica = "select * from ".TABELA_FABRICANTES." where FABRICANTE = '$fabricante'";
		$resultado = $banco->pesquisarBD($verifica);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$banco->fechar();
			return true; // o fabricante ja est� cadastrado
		}
		$banco->fechar();
		return false;
	}

	function cadastrarFabricante($log)
	{
		$banco = new BD;	
		$this->data_operacao = converteDataUsuario(date('d/m/Y'));
		$this->hora_operacao = date('H:i:s');
		$this->cod_acesso = $log->id_acesso;
		$insere = "insert into ".TABELA_FABRICANTES." 
		(FABRICANTE, DESCRICAO, VISIVEL, DATA_OPERACAO, HORA_OPERACAO, COD_ACESSO)values
		('$this->fabricante', '$this->descricao', '$this->visivel', '$this->data_operacao', '$this->hora_operacao', '$this->cod_acesso')";
		if($banco->pesquisarBD($insere)){
			$banco->fechar();
			return true; // fabricante foi cadastrado
		}
		$banco->fechar();
		return false; // erro no cadastro de fabricante
	}

	function alterarFabricante()
	{
		$banco = new BD;	
		$altera = "update ".TABELA_FABRICANTES." set FABRICANTE='$this->
		fabricante',DESCRICAO='this->descricao',VISIVEL='this->visivel' where COD_FABRICANTE = '$this->cod_fabricante'"
		;
		if($banco->pesquisarBD($altera)){
			$banco->fechar();
			return true; // fabricante foi alterado
		}
		$banco->fechar();
		return false; // erro na altera��o do fabricante
	}
	
	function carregarExibicaoFabricantes($qtd=''){
		$banco = new BD();
		$tudo = "select * from ".TABELA_FABRICANTES;
		$resultado = $banco->pesquisarBD($tudo);
		$final = $banco->total_registros($resultado);
		if($final==1) $inicio=0;
		else if($qtd!=''){
			$inicio = $final - $qtd;
			$consulta = "select * from ".TABELA_FABRICANTES." where visivel='S' order by FABRICANTE limit ".$inicio.",".$final;
		}
		else $consulta = "select * from ".TABELA_FABRICANTES." where visivel='S' order by FABRICANTE";
		$this->exibicao = $banco->pesquisarBD($consulta);
	}
	
	function exibirFabricantes(){
		$banco = new BD();
		return $banco->mostra_registros($this->exibicao);
	}	
		
	function excluirFabricante(){
		$banco = new BD;	
		$excluir = "delete from ".TABELA_FABRICANTES." where COD_FABRICANTE='$this->cod_fabricante'";
		if($banco->pesquisarBD($excluir)){
			$banco->fechar();
			return true; // fabricante foi excluido
		}
		$banco->fechar();
		return false; // erro na exclus�o do fabricante
	}
}

/*---------------------------------------------------------------------------------------------------
Classe departamento
---------------------------------------------------------------------------------------------------*/

class departamento{

	var $cod_departamento;
	var $departamento;
	var $depart_visivel;
	var $data_operacao;
	var $hora_operacao;
	var $cod_acesso;
	var $exibicao;
	var $sql;
		
	function departamento($cod_departamento=''){
		$banco = new BD;
		$consulta = "select * from ".TABELA_DEPARTAMENTOS." where 
		COD_DEPARTAMENTO='$cod_departamento'";
		$resultado = $banco->pesquisarBD($consulta);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$this->cod_departamento = $dados["COD_DEPARTAMENTO"];
			$this->departamento = $dados["DEPARTAMENTO"];
			$this->data_operacao = $dados["DATA_OPERACAO"];
			$this->hora_operacao = $dados["HORA_OPERACAO"];
			$this->cod_acesso = $dados["COD_ACESSO"];
			$this->depart_visivel = $dados["VISIVEL"];
		}
		$this->cod_departamento = $cod_departamento;
		$banco->fechar();
	}
	
	function carregarComboDepartamento($submenu = false,$submenu2 = false,$ini_cod=false){
		$banco = new BD;
		$pesquisa = "select * from ".TABELA_DEPARTAMENTOS." where VISIVEL='S' order by DEPARTAMENTO";
		$resultado = $banco->pesquisarBD($pesquisa);
		if(($submenu)&&($submenu2))	$combo = '<select name="departamento" id="departamento" onchange="atualizaCombosProduto(\'carregarComboSubdepartamento\',this.value,\'subdepartamentos\',\'tipos_produtos\')"  alt="requerido" ><option value="">Selecione um departamento</option>';
		else if($submenu)	$combo = '<select name="departamento" id="departamento" onchange="atualizaCombosProduto(\'carregarComboSubdepartamento\',this.value,\'subdepartamentos\')"><option value="">Selecione um departamento</option>';
		else	$combo = '<select name="departamento" id="departamento"><option value="">Selecione um departamento</option>';
		$vazio = true;
		while($dados = $banco->mostra_registros($resultado)){
			if($ini_cod&&($ini_cod==$dados["COD_DEPARTAMENTO"]))	$combo.='<option value="'.$dados["COD_DEPARTAMENTO"].'" selected="selected">'.$dados["DEPARTAMENTO"].'</option>';
			else	$combo.='<option value="'.$dados["COD_DEPARTAMENTO"].'">'.$dados["DEPARTAMENTO"].'</option>';
			$vazio = false;
		}
    	$combo.='</select>';
		if($vazio){
			$banco->fechar();
			return "Nenhum departamento foi cadastrado"; // n�o existem departamentos cadastrados
		}
		$banco->fechar();
		return $combo; // existem departamentos cadastrados
	}
	
	function verificaCadastroDepartamento($departamento){
		$banco = new BD;
		$verifica = "select * from ".TABELA_DEPARTAMENTOS." where DEPARTAMENTO = '$departamento'";
		$resultado = $banco->pesquisarBD($verifica);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$banco->fechar();
			return true; // o departamento ja est� cadastrado
		}
		$banco->fechar();
		return false;
	}

	function cadastrarDepartamento($log)
	{
		$banco = new BD;	
		$this->data_operacao = converteDataUsuario(date('d/m/Y'));
		$this->hora_operacao = date('H:i:s');
		$this->cod_acesso = $log->id_acesso;	
		$insere = "insert into ".TABELA_DEPARTAMENTOS." 
		(DEPARTAMENTO, VISIVEL, DATA_OPERACAO, HORA_OPERACAO, COD_ACESSO)values
		('$this->departamento', '$this->depart_visivel', '$this->data_operacao', '$this->hora_operacao', '$this->cod_acesso')";
		$this->sql = $insere;
		if($banco->pesquisarBD($insere)){
			$banco->fechar();
			return true; // departamento foi cadastrado
		}
		$banco->fechar();
		return false; // erro no cadastro de departamento
	}

	function alterarDepartamento()
	{
		$banco = new BD;	
		$altera = "update ".TABELA_DEPARTAMENTOS." set DEPARTAMENTO='$this->departamento',VISIVEL='$this->depart_visivel' where 
		COD_DEPARTAMENTO = '$this->cod_departamento'";
		if($banco->pesquisarBD($altera)){
			$banco->fechar();
			return true; // departamento foi alterado
		}
		$banco->fechar();
		return false; // erro na altera��o do departamento
	}
	
	function excluirDepartamento(){
		$banco = new BD;	
		$excluir = "delete from ".TABELA_DEPARTAMENTOS." where COD_DEPARTAMENTO='$this->cod_departamento'";
		if($banco->pesquisarBD($excluir)){
			$banco->fechar();
			return true; // departamento foi excluido
		}
		$banco->fechar();
		return false; // erro na exclus�o do departamento
	}
	
	function carregarExibicaoDepartamentos($qtd=''){
		$banco = new BD();
		$tudo = "select * from ".TABELA_DEPARTAMENTOS;
		$resultado = $banco->pesquisarBD($tudo);
		$final = $banco->total_registros($resultado);
		if($final==1) $inicio=0;
		else if($qtd!=''){
			$inicio = $final - $qtd;
			$consulta = "select * from ".TABELA_DEPARTAMENTOS." where VISIVEL='S' order by DEPARTAMENTO limit ".$inicio.",".$final;
		}
		else $consulta = "select * from ".TABELA_DEPARTAMENTOS." where VISIVEL='S' order by DEPARTAMENTO";
		$this->exibicao = $banco->pesquisarBD($consulta);
	}
	
	function exibirDepartamentos(){
		$banco = new BD();
		return $banco->mostra_registros($this->exibicao);
	}
}	
/*---------------------------------------------------------------------------------------------------
Classe subdepartamento derivada da classe departamento
---------------------------------------------------------------------------------------------------*/
class subdepartamento extends departamento{

	var $cod_subdepartamento;
	var $departamento;
	var $sub_visivel;

	function subdepartamento($cod_subdepartamento=''){
		$banco = new BD;
		$consulta = "select * from ".TABELA_SUBDEPARTAMENTOS." where 
		COD_SUBDEPARTAMENTO='$cod_subdepartamento'";
		$resultado = $banco->pesquisarBD($consulta);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$this->cod_subdepartamento = $dados["COD_SUBDEPARTAMENTO"];
			$this->departamento = $dados["DEPARTAMENTO"];
			$this->cod_departamento = $dados["COD_DEPARTAMENTO"];			
			$this->data_operacao = $dados["DATA_OPERACAO"];
			$this->hora_operacao = $dados["HORA_OPERACAO"];
			$this->cod_acesso = $dados["COD_ACESSO"];
			$this->sub_visivel = $dados["VISIVEL"];
			parent::departamento($this->cod_departamento);		
		}
		$this->cod_subdepartamento = $cod_subdepartamento;
		$banco->fechar();
	}
	
	function carregarComboSubdepartamento($cod_departamento='',$submenu = false,$ini_cod=false){
		if($cod_departamento=='') return "Selecione um departamento";
		$banco = new BD;
		$pesquisa = "select * from ".TABELA_SUBDEPARTAMENTOS." where COD_DEPARTAMENTO='$cod_departamento' order by SUBDEPARTAMENTO";
		$resultado = $banco->pesquisarBD($pesquisa);
		if($submenu)	$combo = '<select name="subdepartamento" id="subdepartamento" onchange="atualizaCombosProduto(\'carregarComboTipo_produto\',this.value,\'tipos_produtos\')"  alt="requerido" ><option value="">Selecione um subdepartamento</option>';
		else	$combo = '<select name="subdepartamento" id="subdepartamento"><option value="">Selecione um subdepartamento</option>';
		$vazio = true;
		while($dados = $banco->mostra_registros($resultado)){
			if($ini_cod&&($ini_cod==$dados["COD_SUBDEPARTAMENTO"]))	$combo.='<option value="'.$dados["COD_SUBDEPARTAMENTO"].'" selected="selected">'.$dados["SUBDEPARTAMENTO"].'</option>';
			else				
			$combo.='<option value="'.$dados["COD_SUBDEPARTAMENTO"].'">'.$dados["SUBDEPARTAMENTO"].'</option>';
			$vazio = false;
		}
    	$combo.='</select>';
		if($vazio){
			$banco->fechar();
			return "Nenhum subdepartamento foi cadastrado para este departamento"; // n�o existem subdepartamentos cadastrados
		}
		$banco->fechar();
		return $combo; // existem subdepartamentos cadastrados
	}

	function verificaCadastroSubdepartamento($subdepartamento){
		$banco = new BD;
		$verifica = "select * from ".TABELA_SUBDEPARTAMENTOS." where SUBDEPARTAMENTO = '$subdepartamento'";
		$this->sql = $verifica;
		$resultado = $banco->pesquisarBD($verifica);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$banco->fechar();
			return true; // o subdepartamento ja est� cadastrado
		}
		$banco->fechar();
		return false;
	}

	function cadastrarSubdepartamento($log)
	{
		$banco = new BD;	
		$this->data_operacao = converteDataUsuario(date('d/m/Y'));
		$this->hora_operacao = date('H:i:s');
		$this->cod_acesso = $log->id_acesso;
		$insere = "insert into ".TABELA_SUBDEPARTAMENTOS." 
		(SUBDEPARTAMENTO, COD_DEPARTAMENTO, VISIVEL, DATA_OPERACAO, HORA_OPERACAO, COD_ACESSO)values
		('$this->subdepartamento', '$this->cod_departamento', '$this->sub_visivel', '$this->data_operacao', '$this->hora_operacao', '$this->cod_acesso')";
		$this->sql = $insere;
		if($banco->pesquisarBD($insere)){
			$banco->fechar();
			return true; // subdepartamento foi cadastrado
		}
		$banco->fechar();
		return false; // erro no cadastro de subdepartamento
	}

	function alterarSubdepartamento()
	{
		$banco = new BD;	
		$altera = "update ".TABELA_SUBDEPARTAMENTOS." set SUBDEPARTAMENTO='$this->subdepartamento',VISIVEL='$this->sub_visivel' where COD_SUBDEPARTAMENTO = '$this->cod_subdepartamento'";
		if($banco->pesquisarBD($altera)){
			$banco->fechar();
			return true; // departamento foi alterado
		}
		$banco->fechar();
		return false; // erro na altera��o do departamento
	}
	
	function excluirSubdepartamento(){
		$banco = new BD;	
		$excluir = "delete from ".TABELA_SUBDEPARTAMENTOS." where COD_SUBDEPARTAMENTO='$this->cod_subdepartamento'";
		if($banco->pesquisarBD($excluir)){
			$banco->fechar();
			return true; // subdepartamento foi excluido
		}
		$banco->fechar();
		return false; // erro na exclus�o do subdepartamento
	}
}
/*---------------------------------------------------------------------------------------------------
Classe tipo_produto derivada da classe subdepartamento
---------------------------------------------------------------------------------------------------*/
class tipo_produto extends subdepartamento{

	var $cod_tipo_produto;
	var $tipo_produto;
	var $campos_produto = array();
	var $valores_campos_produto = array();
	var $cod_valores = array();
	var $tipo_visivel;
	
	function tipo_produto($cod_tipo_produto=''){
		$banco = new BD;
		$consulta = "select * from ".TABELA_TIPO_PRODUTOS." where 
		COD_TIPO_PRODUTO='$cod_tipo_produto'";
		$resultado = $banco->pesquisarBD($consulta);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$this->cod_tipo_produto = $dados["COD_TIPO_PRODUTO"];
			$this->tipo_produto = $dados["TIPO_PRODUTO"];
			$this->cod_subdepartamento = $dados["COD_SUBDEPARTAMENTO"];			
			$this->data_operacao = $dados["DATA_OPERACAO"];
			$this->hora_operacao = $dados["HORA_OPERACAO"];
			$this->cod_acesso = $dados["COD_ACESSO"];
			$this->tipo_visivel  =$dados["VISIVEL"];
			$consulta = "select * from ".TABELA_CAMPOS_PRODUTO_POR_TIPO_PRODUTO." left join ".TABELA_CAMPOS_PRODUTO." on ".TABELA_CAMPOS_PRODUTO_POR_TIPO_PRODUTO.".cod_campo_produto = ".TABELA_CAMPOS_PRODUTO.".cod_campo_produto left join ".TABELA_TIPO_PRODUTOS." on ".TABELA_CAMPOS_PRODUTO_POR_TIPO_PRODUTO.".cod_tipo_produto = ".TABELA_TIPO_PRODUTOS.".cod_tipo_produto WHERE ".TABELA_TIPO_PRODUTOS.".COD_TIPO_PRODUTO = '$this->cod_tipo_produto' order by ".TABELA_CAMPOS_PRODUTO.".campo_produto";
			$resultado = $banco->pesquisarBD($consulta);
			$cont=0;
			while($dados = $banco->mostra_registros($resultado)){
				$this->campos_produto[$cont]["CAMPO_PRODUTO"] = $dados["CAMPO_PRODUTO"];
				$this->campos_produto[$cont]["COD_CAMPO_PRODUTO_POR_TIPO_PRODUTO"] = $dados["COD_CAMPO_PRODUTO_POR_TIPO_PRODUTO"];				
				$this->campos_produto[$cont]["COD_CAMPO_PRODUTO"] = $dados["COD_CAMPO_PRODUTO"];				
				$cont++;				
			}
			$consulta = "select * from ".TABELA_VALORES_CAMPOS_PRODUTOS." left join ".TABELA_CAMPOS_PRODUTO_POR_TIPO_PRODUTO." on ".TABELA_CAMPOS_PRODUTO_POR_TIPO_PRODUTO.".COD_CAMPO_PRODUTO_POR_TIPO_PRODUTO = ".TABELA_VALORES_CAMPOS_PRODUTOS.".COD_CAMPO_PRODUTO_POR_TIPO_PRODUTO WHERE ".TABELA_CAMPOS_PRODUTO_POR_TIPO_PRODUTO.".COD_TIPO_PRODUTO = '$this->cod_tipo_produto' order by VALOR_CAMPO";
			$resultado = $banco->pesquisarBD($consulta);
			$cont=0;
			$indice_anterior = '';
			while($dados = $banco->mostra_registros($resultado)){
				$indice = $dados["COD_CAMPO_PRODUTO_POR_TIPO_PRODUTO"];
				if($indice!=$indice_anterior){
					$cont=0;
					$indice_anterior = $indice;
				}
				$this->valores_campos_produto[$indice][$cont] = $dados["VALOR_CAMPO"];
				$this->cod_valores[$indice][$cont] = $dados["COD_VALOR_CAMPO_PRODUTO"];
				$cont++;
			}			
			parent::subdepartamento($this->cod_subdepartamento);		
		}
		$this->cod_tipo_produto = $cod_tipo_produto;
		$banco->fechar();
	}
	
	function carregarExibicaoTipoproduto($qtd=''){
		$banco = new BD();
		$tudo = "select * from ".TABELA_TIPO_PRODUTOS;
		$resultado = $banco->pesquisarBD($tudo);
		$final = $banco->total_registros($resultado);
		if($final==1) $inicio=0;
		else if($qtd!=''){
			$inicio = $final - $qtd;
			$consulta = "select * from ".TABELA_TIPO_PRODUTOS." where VISIVEL='S' order by TIPO_PRODUTO limit ".$inicio.",".$final;
		}
		else $consulta = "select * from ".TABELA_TIPO_PRODUTOS." where VISIVEL='S' order by TIPO_PRODUTO";
		$this->exibicao = $banco->pesquisarBD($consulta);
	}
	
	function exibirTipo_produto(){
		$banco = new BD();
		return $banco->mostra_registros($this->exibicao);
	}	
	
	function carregarComboCamposDisponiveis($cod_tipo_produto='',$submenu = false,$ini_cod=''){
		$banco = new BD;
		$pesquisa = "select * from ".TABELA_CAMPOS_PRODUTO." order by CAMPO_PRODUTO";
		$resultado = $banco->pesquisarBD($pesquisa);
		if($submenu)	$combo = '<select name="campo_produto" id="campo_produto" onchange="atualizaCombosProduto(\'carregarComboValores\',this.value,\'valores_campos_produtos\')"><option value="">Selecione um campo</option>';
		else	$combo = '<select name="campo_produto" id="campo_produto"><option value="">Selecione um campo</option>';
		$vazio = true;
		while($dados = $banco->mostra_registros($resultado)){
			$combo.='<option value="'.$dados["COD_CAMPO_PRODUTO"].'">'.$dados["CAMPO_PRODUTO"].'</option>';
			$vazio = false;
		}
    	$combo.='</select>';
		if($vazio){
			$banco->fechar();
			return "Nenhum campo foi cadastrado"; // n�o existem campos cadastrados
		}
		$banco->fechar();
		return $combo; // existem campos cadastrados
	}	
	
	function carregarComboCampos($cod_tipo_produto='',$submenu = false,$ini_cod=false){
		$banco = new BD;
		$pesquisa = "select ".TABELA_CAMPOS_PRODUTO.".CAMPO_PRODUTO,".TABELA_CAMPOS_PRODUTO_POR_TIPO_PRODUTO.".COD_CAMPO_PRODUTO_POR_TIPO_PRODUTO from ".TABELA_CAMPOS_PRODUTO_POR_TIPO_PRODUTO." inner join ".TABELA_CAMPOS_PRODUTO." on ".TABELA_CAMPOS_PRODUTO_POR_TIPO_PRODUTO.".COD_CAMPO_PRODUTO = ".TABELA_CAMPOS_PRODUTO.".COD_CAMPO_PRODUTO where ".TABELA_CAMPOS_PRODUTO_POR_TIPO_PRODUTO.".COD_TIPO_PRODUTO='$cod_tipo_produto' order by CAMPO_PRODUTO";
		$resultado = $banco->pesquisarBD($pesquisa);
		if($submenu)	$combo = '<select name="campo_produto" id="campo_produto" onchange="atualizaCombosProduto(\'carregarComboValores\',this.value,\'valores_campos_produtos\')"><option value="">Selecione um campo</option>';
		else	$combo = '<select name="campo_produto" id="campo_produto"><option value="">Selecione um campo</option>';
		$vazio = true;
		while($dados = $banco->mostra_registros($resultado)){
			if($ini_cod&&($ini_cod==$dados["COD_CAMPO_PRODUTO_POR_TIPO_PRODUTO"]))	$combo.='<option value="'.$dados["COD_CAMPO_PRODUTO_POR_TIPO_PRODUTO"].'" selected="selected">'.$dados["CAMPO_PRODUTO"].'</option>';
			else				
			$combo.='<option value="'.$dados["COD_CAMPO_PRODUTO_POR_TIPO_PRODUTO"].'">'.$dados["CAMPO_PRODUTO"].'</option>';
			$vazio = false;
		}
    	$combo.='</select>';
		if($vazio){
			$banco->fechar();
			return "Nenhum campo foi cadastrado"; // n�o existem campos cadastrados
		}
		$banco->fechar();
		return $combo; // existem campos cadastrados
	}	
	
	function carregarComboTipo_produto($cod_subdepartamento='',$submenu = false,$ini_cod=false){
		if($cod_subdepartamento=='') return "Selecione um subdepartamento";
		$banco = new BD;
		$pesquisa = "select * from ".TABELA_TIPO_PRODUTOS." where COD_SUBDEPARTAMENTO='$cod_subdepartamento' order by TIPO_PRODUTO";
		$resultado = $banco->pesquisarBD($pesquisa);
		if($submenu)	$combo = '<select name="tipo_produto" id="tipo_produto" onchange="atualizaCombosProduto(\'carregarComboCampos\',this.value,\'campos_produtos\')" alt="requerido" ><option value="">Selecione um tipo de produto</option>';
		else $combo = '<select name="tipo_produto" id="tipo_produto"><option value="">Selecione um tipo de produto</option>';
		$vazio = true;
		while($dados = $banco->mostra_registros($resultado)){
			if($ini_cod&&($ini_cod==$dados["COD_TIPO_PRODUTO"]))	$combo.='<option value="'.$dados["COD_TIPO_PRODUTO"].'" selected="selected">'.$dados["TIPO_PRODUTO"].'</option>';
			else				
			$combo.='<option value="'.$dados["COD_TIPO_PRODUTO"].'">'.$dados["TIPO_PRODUTO"].'</option>';
			$vazio = false;
		}
    	$combo.='</select>';
		if($vazio){
			$banco->fechar();
			return "Nenhum tipo de produto foi cadastrado para este subdepartamento"; // n�o existem tipo_produto cadastrados
		}
		$banco->fechar();
		return $combo; // existem tipo_produto cadastrados
	}	
	
	function verificaCadastroTipo_produto($tipo_produto){
		$banco = new BD;
		$verifica = "select * from ".TABELA_TIPO_PRODUTOS." where TIPO_PRODUTO = '$tipo_produto'";
		$resultado = $banco->pesquisarBD($verifica);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$banco->fechar();
			return true; // o tipo_produto ja est� cadastrado
		}
		$banco->fechar();
		return false;
	}
	
	function verificaCadastroCampo_produto($campo_produto){
		$banco = new BD;
		$verifica = "select * from ".TABELA_CAMPOS_PRODUTO." where CAMPO_PRODUTO =
		'$campo_produto'";
		$resultado = $banco->pesquisarBD($verifica);
		$dados = $banco->mostra_registros($resultado);
		if($dados)	return $dados["COD_CAMPO_PRODUTO"]; // o campo_produto ja est� cadastrado
		$banco->fechar();
		return false;
	}	
	
	function verificaCadastroCampo_produtoTipo_produto($campo_produto='',$cod_campo=''){
		$banco = new BD;
		if($cod_campo=='')	$verifica = "select ".TABELA_CAMPOS_PRODUTO.".COD_CAMPO_PRODUTO from ".TABELA_CAMPOS_PRODUTO." left join ".TABELA_CAMPOS_PRODUTO_POR_TIPO_PRODUTO." on ".TABELA_CAMPOS_PRODUTO_POR_TIPO_PRODUTO.".cod_campo_produto =
 ".TABELA_CAMPOS_PRODUTO.".cod_campo_produto where ".TABELA_CAMPOS_PRODUTO.".CAMPO_PRODUTO =
		'$campo_produto' and ".TABELA_CAMPOS_PRODUTO_POR_TIPO_PRODUTO.".COD_TIPO_PRODUTO='$this->cod_tipo_produto'";
		else	$verifica = "select * from ".TABELA_CAMPOS_PRODUTO_POR_TIPO_PRODUTO." where COD_CAMPO_PRODUTO='$cod_campo' and COD_TIPO_PRODUTO='$this->cod_tipo_produto'";
		$this->sql = $verifica;
		$resultado = $banco->pesquisarBD($verifica);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$banco->fechar();
			return true; // o campo_produto ja est� cadastrado para este tipo_produto
		}
		$banco->fechar();
		return false;
	}	

	function verificaValorProduto($valor,$campo){
		$banco = new BD;
		$verifica = "select * from ".TABELA_VALORES_CAMPOS_PRODUTOS." where VALOR_CAMPO =
		'$valor' and COD_CAMPO_PRODUTO_POR_TIPO_PRODUTO='$campo'";
		$this->sql = $verifica;
		$resultado = $banco->pesquisarBD($verifica);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$banco->fechar();
			return true; // o valor do campo_produto ja est� cadastrado para este tipo_produto
		}
		$banco->fechar();
		return false;
	}

	function cadastrarTipo_produto($log)
	{
		$banco = new BD;	
		$this->data_operacao = converteDataUsuario(date('d/m/Y'));
		$this->hora_operacao = date('H:i:s');
		$this->cod_acesso = $log->id_acesso;
		$insere = "insert into ".TABELA_TIPO_PRODUTOS." 
		(TIPO_PRODUTO, COD_SUBDEPARTAMENTO, VISIVEL, DATA_OPERACAO, HORA_OPERACAO, COD_ACESSO)values
		('$this->tipo_produto', '$this->cod_subdepartamento', '$this->tipo_visivel', '$this->data_operacao', '$this->hora_operacao', '$this->cod_acesso')";
		$this->sql = $insere;
		if($banco->pesquisarBD($insere)){
			$banco->fechar();
			return true; // tipo_produto foi cadastrado
		}
		$banco->fechar();
		return false; // erro no cadastro de tipo_produto
	}
	
	function cadastrarCampo_produto($campo_produto,$log)
	{
		$banco = new BD;	
		$this->data_operacao = converteDataUsuario(date('d/m/Y'));
		$this->hora_operacao = date('H:i:s');
		$this->cod_acesso = $log->id_acesso;		
		$insere = "insert into ".TABELA_CAMPOS_PRODUTO." 
		(CAMPO_PRODUTO, DATA_OPERACAO, HORA_OPERACAO, COD_ACESSO)values
		('$campo_produto', '$this->data_operacao', '$this->hora_operacao', '$this->
		cod_acesso')";
		if($banco->pesquisarBD($insere)){
			$banco->fechar();
			return novoCodigo(TABELA_CAMPOS_PRODUTO,"COD_CAMPO_PRODUTO"); // campo_produto foi cadastrado
		}
		$banco->fechar();
		return false; // erro no cadastro de campo_produto
	}
	
	function cadastrarValorProduto($valor,$campo,$log)
	{
		$banco = new BD;	
		$this->data_operacao = converteDataUsuario(date('d/m/Y'));
		$this->hora_operacao = date('H:i:s');
		$this->cod_acesso = $log->id_acesso;		
		$insere = "insert into ".TABELA_VALORES_CAMPOS_PRODUTOS." 
		(VALOR_CAMPO, COD_CAMPO_PRODUTO_POR_TIPO_PRODUTO, DATA_OPERACAO, HORA_OPERACAO, COD_ACESSO)values
		('$valor', '$campo', '$this->data_operacao', '$this->hora_operacao', '$this->cod_acesso')";
		$this->sql = $insere;
		if($banco->pesquisarBD($insere)){
			$banco->fechar();
			return true; // valor_campo_produto foi cadastrado
		}
		$banco->fechar();
		return false; // erro no cadastro de valor_campo_produto
	}
	
	function cadastrarCampo_produtoTipo_produto($campo_produto='',$cod_campo='',$log)
	{
		$banco = new BD;	
		$this->data_operacao = converteDataUsuario(date('d/m/Y'));		
		$this->hora_operacao = date('H:i:s');
		$this->cod_acesso = $log->id_acesso;
		if($cod_campo==''){
			$cod_campo_produto = $this->verificaCadastroCampo_produto($campo_produto);
			if(!$cod_campo_produto)	$cod_campo_produto = $this->cadastrarCampo_produto($campo_produto);
			$insere = "insert into ".TABELA_CAMPOS_PRODUTO_POR_TIPO_PRODUTO." (COD_TIPO_PRODUTO,COD_CAMPO_PRODUTO,DATA_OPERACAO,HORA_OPERACAO,COD_ACESSO)values('$this->cod_tipo_produto','$cod_campo_produto','$this->data_operacao','$this->hora_operacao','$this->cod_acesso')";
		}
		else $insere = "insert into ".TABELA_CAMPOS_PRODUTO_POR_TIPO_PRODUTO." (COD_TIPO_PRODUTO,COD_CAMPO_PRODUTO,DATA_OPERACAO,HORA_OPERACAO,COD_ACESSO)values('$this->cod_tipo_produto','$cod_campo','$this->data_operacao','$this->hora_operacao','$this->cod_acesso')";
		//$this->sql = $insere;
		if($banco->pesquisarBD($insere)){
			$banco->fechar();
			return true; // campo_produto foi cadastrado para tipo_produto
		}
		$banco->fechar();
		return false; // erro no cadastro de campo_produto para tipo produto
	}	

	function alterarTipo_produto()
	{
		$banco = new BD;	
		$altera = "update ".TABELA_TIPO_PRODUTOS." set TIPO_PRODUTO='$this->tipo_produto'
		where COD_TIPO_PRODUTO = '$this->cod_tipo_produto'";
		if($banco->pesquisarBD($altera)){
			$banco->fechar();
			return true; // departamento foi alterado
		}
		$banco->fechar();
		return false; // erro na altera��o do departamento
	}
	
	function excluirTipo_produto(){
		$banco = new BD;	
		$excluir = "delete from ".TABELA_TIPO_PRODUTOS." where COD_TIPO_PRODUTO='$this->cod_tipo_produto'";
		if($banco->pesquisarBD($excluir)){
			$banco->fechar();
			return true; // subdepartamento foi excluido
		}
		$banco->fechar();
		return false; // erro na exclus�o do subdepartamento
	}
}
/*---------------------------------------------------------------------------------------------------
Classe produto derivada da classe tipo_produto
---------------------------------------------------------------------------------------------------*/
class produto extends tipo_produto{

	var $cod_produto;
	var $referencia;
	var $modelo;
	var $nome;
	var $descricao;
	var $cod_fabricante;
	var $codigo_barras;
	var $data_cadastro;
	var $descontinuado;
	var $data_descontinuado;
	var $peso_liquido;
	var $cod_empresa;
	var $qtd_estoque;
	var $estoque_minimo;
	var $valor;
	var $visivel;
	var $fotos = array();
	var $campos = array();
	var $valores_campos = array();
	var $cod_valor = array();
	var $data_operacao;
	var $hora_operacao;
	var $cod_acesso;
	var $ultimos;
	var $maisVendidos;
	
	function getDataCadastro(){
		return converteDataBanco($this->data_cadastro);
	}
	
	function produto($cod_tipo_produto='',$cod_produto=''){
		if($cod_tipo_produto!='')	parent::tipo_produto($cod_tipo_produto);
		$banco = new BD;
		$consulta = "select * from ".TABELA_PRODUTOS." where COD_PRODUTO='$cod_produto'";
		$resultado = $banco->pesquisarBD($consulta);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$consulta = "select * from ".TABELA_ESTOQUE." where 
			COD_PRODUTO='$cod_produto'";
			$resultado = $banco->pesquisarBD($consulta);
			$estoque_bd = $banco->mostra_registros($resultado);
			$consulta = "select FABRICANTE from ".TABELA_FABRICANTES." INNER JOIN ".TABELA_PRODUTOS." ON ".TABELA_FABRICANTES.".COD_FABRICANTE=".TABELA_PRODUTOS.".COD_FABRICANTE WHERE COD_PRODUTO='$cod_produto'";
			$resultado = $banco->pesquisarBD($consulta);
			$fabricante = $banco->mostra_registros($resultado);
			$consulta = "select * from ".TABELA_FOTOS_PRODUTO." where 
			COD_PRODUTO='$cod_produto'";
			$resultado = $banco->pesquisarBD($consulta);
			$cont=0;
			while($fotos_bd = $banco->mostra_registros($resultado)){
				$this->fotos[$cont] = $fotos_bd["CAMINHO"];
				$cont++;
			}
			$consulta = "select ".TABELA_VALORES.".VALOR_CAMPO,".TABELA_CAMPOS_PRODUTO.".CAMPO_PRODUTO,
".TABELA_VALORES_CAMPOS_PRODUTOS.".VALOR_CAMPO as VALORDEFINIDO from ".TABELA_VALORES." 
inner join ".TABELA_CAMPOS_PRODUTO." on ".TABELA_CAMPOS_PRODUTO.".COD_CAMPO_PRODUTO=
".TABELA_VALORES.".COD_CAMPO LEFT JOIN ".TABELA_VALORES_CAMPOS_PRODUTOS." 
ON ".TABELA_VALORES_CAMPOS_PRODUTOS.".COD_VALOR_CAMPO_PRODUTO=".TABELA_VALORES.".COD_VALOR where 
".TABELA_VALORES.".COD_PRODUTO='$cod_produto'";
			$this->sql = $consulta;
			$resultado = $banco->pesquisarBD($consulta);
			$cont=0;
			while($valores_bd = $banco->mostra_registros($resultado)){
				$this->campos[$cont] = $valores_bd["CAMPO_PRODUTO"];
				if($valores_bd["VALOR_CAMPO"]==''){
					$this->valores_campos[$cont] = $valores_bd["VALORDEFINIDO"];
				}
				else{
					$this->valores_campos[$cont] = $valores_bd["VALOR_CAMPO"];
				}
				$cont++;
			}			
			$this->cod_produto = $dados["COD_PRODUTO"];
			$this->referencia = $dados["REFERENCIA"];
			$this->modelo = $dados["MODELO"];
			$this->nome = $dados["NOME"];
			$this->descricao = $dados["DESCRICAO"];
			$this->cod_fabricante = $fabricante["FABRICANTE"];
			$this->cod_tipo_produto = $dados["COD_TIPO_PRODUTO"];	
			$this->codigo_barras = $dados["CODIGO_BARRAS"];
			$this->data_cadastro = $dados["DATA_CADASTRO"];
			$this->descontinuado = $dados["DESCONTINUADO"];
			$this->peso_liquido = $dados["PESO_LIQUIDO"];
			$this->cod_empresa = $estoque_bd["COD_EMPRESA"];
			$this->qtd_estoque = $estoque_bd["ESTOQUE"];			
			$this->estoque_minimo = $estoque_bd["ESTOQUE_MINIMO"];
			$this->valor = $estoque_bd["VALOR_PRODUTO"];
			$this->visivel = $dados["VISIVEL"];
			$this->data_operacao = $dados["DATA_OPERACAO"];
			$this->hora_operacao = $dados["HORA_OPERACAO"];
			$this->cod_acesso = $dados["COD_ACESSO"];	
			parent::tipo_produto($this->cod_tipo_produto);		
		}
		$this->cod_produto = $cod_produto;
		$banco->fechar();
	}
	
	function verificaCadastroProduto($modelo){
		$banco = new BD;
		$verifica = "select * from ".TABELA_PRODUTOS." where MODELO = '$modelo'";
		$resultado = $banco->pesquisarBD($verifica);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$banco->fechar();
			return true; // o produto ja est� cadastrado
		}
		$banco->fechar();
		return false;
	}

	function cadastrarProduto($log)
	{
		$banco = new BD;	
		$this->data_operacao = converteDataUsuario(date('d/m/Y'));
		$this->data_cadastro = $this->data_operacao;
		$this->hora_operacao = date('H:i:s');
		$this->cod_acesso = $log->id_acesso;
		$insere_produto = "insert into ".TABELA_PRODUTOS." 
		(REFERENCIA, MODELO, NOME, DESCRICAO, COD_FABRICANTE,	COD_TIPO_PRODUTO, CODIGO_BARRAS, DATA_CADASTRO, PESO_LIQUIDO, VISIVEL, DATA_OPERACAO, HORA_OPERACAO, COD_ACESSO)values('$this->referencia', '$this->modelo', '$this->nome', '$this->descricao', '$this->cod_fabricante',	'$this->cod_tipo_produto', '$this->codigo_barras', '$this->data_cadastro', '$this->peso_liquido', '$this->visivel', '$this->data_operacao', '$this->hora_operacao', '$this->cod_acesso')";
		if($banco->pesquisarBD($insere_produto)){
			$this->cod_produto = novoCodigo(TABELA_PRODUTOS,"COD_PRODUTO");
		$insere_estoque="insert into ".TABELA_ESTOQUE." 
		(COD_PRODUTO, COD_EMPRESA, ESTOQUE, ESTOQUE_MINIMO, VALOR_PRODUTO, DATA_OPERACAO, HORA_OPERACAO, COD_ACESSO)values('$this->cod_produto', '$this->cod_empresa', '$this->qtd_estoque', '$this->estoque_minimo', '$this->valor', '$this->data_operacao', '$this->hora_operacao', '$this->cod_acesso')";		
				$cont=0;
		while($this->campos_produto[$cont]){
			$insere_valores="insert into ".TABELA_VALORES." 
		(COD_VALOR, COD_CAMPO, COD_PRODUTO, VALOR_CAMPO, DATA_OPERACAO, HORA_OPERACAO, COD_ACESSO)values('".$this->cod_valor[$cont]."', '".$this->campos[$cont]."', '$this->cod_produto', '".$this->campos_valores[$cont]."', '$this->data_operacao', '$this->hora_operacao', '$this->cod_acesso')";
			$this->sql = $insere_valores;
			if(!$banco->pesquisarBD($insere_valores)) return false;
			$cont++;
		}
		$cont=0;
		while($this->fotos[$cont]){	
			$insere_foto="insert into ".TABELA_FOTOS_PRODUTO." 
		(CAMINHO, COD_PRODUTO, DATA_OPERACAO, HORA_OPERACAO, COD_ACESSO)values('".$this->fotos[$cont]."', '$this->cod_produto', '$this->data_operacao', '$this->hora_operacao', '$this->cod_acesso')";
			$this->sql = $insere_foto;
			if(!$banco->pesquisarBD($insere_foto)) return false;
			$cont++;
		}
			if($banco->pesquisarBD($insere_estoque))
			$banco->fechar();
			return true; // produto foi cadastrado
		}
		$banco->fechar();
		return false; // erro no cadastro de produto
	}
	
	function receberFoto($foto){
		$destino = 'fotos/produtos/'.$this->referencia.'_'.$foto['name'];
		array_push($this->fotos,$destino);
		if(!move_uploaded_file($foto['tmp_name'],$destino))	exit("Erro copiando fotografia:    ".$foto['tmp_name'].'//'.$destino );
	}
	
	function carregarExibicaoProdutos($qtd='',$filtro='',$valor_filtro=''){
		$banco = new BD();
		$tudo = "select * from ".TABELA_PRODUTOS;
		$resultado = $banco->pesquisarBD($tudo);
		$final = $banco->total_registros($resultado);
		if($final==1) $inicio=0;
		switch($filtro){
			// select por departamento
			case 'D':$consulta = "select * from ".TABELA_PRODUTOS." inner join ".TABELA_FOTOS_PRODUTO." on ".TABELA_PRODUTOS.".COD_PRODUTO=
".TABELA_FOTOS_PRODUTO.".COD_PRODUTO inner join ".TABELA_ESTOQUE." on ".TABELA_ESTOQUE.".COD_PRODUTO=
".TABELA_PRODUTOS.".COD_PRODUTO inner join ".TABELA_TIPO_PRODUTOS." on ".TABELA_PRODUTOS.".COD_TIPO_PRODUTO=
".TABELA_TIPO_PRODUTOS.".COD_TIPO_PRODUTO inner join ".TABELA_SUBDEPARTAMENTOS." on 
".TABELA_SUBDEPARTAMENTOS.".COD_SUBDEPARTAMENTO=".TABELA_TIPO_PRODUTOS.".COD_SUBDEPARTAMENTO
inner join ".TABELA_DEPARTAMENTOS." on ".TABELA_DEPARTAMENTOS.".COD_DEPARTAMENTO=
".TABELA_SUBDEPARTAMENTOS.".COD_DEPARTAMENTO where ".TABELA_PRODUTOS.".visivel='S' 
and ".TABELA_DEPARTAMENTOS.".COD_DEPARTAMENTO='$valor_filtro' order by ".TABELA_PRODUTOS.".NOME";
					 break;
			// select por subdepartamento					 
			case 'S':$consulta = "select * from ".TABELA_PRODUTOS." inner join ".TABELA_FOTOS_PRODUTO." on ".TABELA_PRODUTOS.".COD_PRODUTO=
".TABELA_FOTOS_PRODUTO.".COD_PRODUTO inner join ".TABELA_ESTOQUE." on ".TABELA_ESTOQUE.".COD_PRODUTO=
".TABELA_PRODUTOS.".COD_PRODUTO inner join ".TABELA_TIPO_PRODUTOS." on ".TABELA_PRODUTOS.".COD_TIPO_PRODUTO=
".TABELA_TIPO_PRODUTOS.".COD_TIPO_PRODUTO inner join ".TABELA_SUBDEPARTAMENTOS." on 
".TABELA_SUBDEPARTAMENTOS.".COD_SUBDEPARTAMENTO=".TABELA_TIPO_PRODUTOS.".COD_SUBDEPARTAMENTO where ".TABELA_PRODUTOS.".visivel='S' 
and ".TABELA_SUBDEPARTAMENTOS.".COD_SUBDEPARTAMENTO='$valor_filtro' order by ".TABELA_PRODUTOS.".NOME";
					 break;
			// select por tipo de produto					 
			case 'T':$consulta = "select * from ".TABELA_PRODUTOS." inner join ".TABELA_FOTOS_PRODUTO." on ".TABELA_PRODUTOS.".COD_PRODUTO=
".TABELA_FOTOS_PRODUTO.".COD_PRODUTO inner join ".TABELA_ESTOQUE." on ".TABELA_ESTOQUE.".COD_PRODUTO=
".TABELA_PRODUTOS.".COD_PRODUTO inner join ".TABELA_TIPO_PRODUTOS." on ".TABELA_PRODUTOS.".COD_TIPO_PRODUTO=
".TABELA_TIPO_PRODUTOS.".COD_TIPO_PRODUTO where ".TABELA_PRODUTOS.".visivel='S' 
and ".TABELA_TIPO_PRODUTOS.".COD_TIPO_PRODUTO='$valor_filtro' order by ".TABELA_PRODUTOS.".NOME";
					 break;
			// select por fabricante					 					
			case 'F':$consulta = "select * from ".TABELA_PRODUTOS." inner join ".TABELA_FOTOS_PRODUTO." on ".TABELA_PRODUTOS.".COD_PRODUTO=
".TABELA_FOTOS_PRODUTO.".COD_PRODUTO inner join ".TABELA_ESTOQUE." on ".TABELA_ESTOQUE.".COD_PRODUTO=
".TABELA_PRODUTOS.".COD_PRODUTO where ".TABELA_PRODUTOS.".visivel='S' 
and ".TABELA_PRODUTOS.".COD_FABRICANTE='$valor_filtro' order by ".TABELA_PRODUTOS.".NOME";
					 break;
			// select por busca
			case 'B':$consulta = "select * from ".TABELA_PRODUTOS." inner join ".TABELA_FOTOS_PRODUTO." on  ".TABELA_PRODUTOS.".COD_PRODUTO=".TABELA_FOTOS_PRODUTO.".COD_PRODUTO inner join ".TABELA_ESTOQUE." on ".TABELA_ESTOQUE.".COD_PRODUTO=".TABELA_PRODUTOS.".COD_PRODUTO where ".TABELA_PRODUTOS.".visivel='S' and ".TABELA_PRODUTOS.".NOME like ('%'\"$valor_filtro\"'%') order by ".TABELA_PRODUTOS.".NOME";
					 break;					 				 			
			default:$consulta = "select * from ".TABELA_PRODUTOS." inner join ".TABELA_FOTOS_PRODUTO." on  ".TABELA_PRODUTOS.".COD_PRODUTO=".TABELA_FOTOS_PRODUTO.".COD_PRODUTO inner join ".TABELA_ESTOQUE." on ".TABELA_ESTOQUE.".COD_PRODUTO=".TABELA_PRODUTOS.".COD_PRODUTO where ".TABELA_PRODUTOS.".visivel='S' order by ".TABELA_PRODUTOS.".NOME";
					 break;
		}		
		if(($qtd!='')&&($qtd<=$final)){
			$consulta.=" limit 0,".$qtd*2;
		}
		$this->sql = $consulta;
		$this->exibicao = $banco->pesquisarBD($consulta);
	}
	
	function ultimosCadastrados($qtd=''){
	$banco = new BD();
	$tudo = "select * from ".TABELA_PRODUTOS;
	$resultado = $banco->pesquisarBD($tudo);
	$final = $banco->total_registros($resultado);
	if($final==1) $inicio=0;	
	$consulta = "select * from ".TABELA_PRODUTOS." inner join ".TABELA_FOTOS_PRODUTO." on  ".TABELA_PRODUTOS.".COD_PRODUTO=".TABELA_FOTOS_PRODUTO.".COD_PRODUTO inner join ".TABELA_ESTOQUE." on ".TABELA_ESTOQUE.".COD_PRODUTO=".TABELA_PRODUTOS.".COD_PRODUTO where ".TABELA_PRODUTOS.".visivel='S' order by ".TABELA_PRODUTOS.".COD_PRODUTO desc";
	if(($qtd!='')&&($qtd<=$final)){
		$consulta.=" limit 0,".$qtd*2;
	}
	$this->sql = $consulta;
	$this->ultimos = $banco->pesquisarBD($consulta);
	}
	
	function maisVendidos($qtd=''){
	$banco = new BD();
	$tudo = "select * from ".TABELA_PRODUTOS_PEDIDO;
	$resultado = $banco->pesquisarBD($tudo);
	$final = $banco->total_registros($resultado);
	if($final==1) $inicio=0;	
	$consulta = "select produtos.NOME,produtos_pedido.COD_PRODUTO,count(produtos_pedido.COD_PRODUTO) as QUANTIDADE from ".TABELA_PRODUTOS_PEDIDO." inner join ".TABELA_PRODUTOS." on  ".TABELA_PRODUTOS.".COD_PRODUTO=".TABELA_PRODUTOS_PEDIDO.".COD_PRODUTO group by ".TABELA_PRODUTOS_PEDIDO.".COD_PRODUTO order by QUANTIDADE desc";
	if(($qtd!='')&&($qtd<=$final)){
		$consulta.=" limit 0,".$qtd;
	}
	$this->sql = $consulta;
	$this->maisVendidos = $banco->pesquisarBD($consulta);
	}	
	
	function exibirProdutos($ultimos = false,$maisVendidos =false){
		if($ultimos){
			$banco = new BD();
			$retorno = $banco->mostra_registros($this->ultimos);
			$banco->mostra_registros($this->ultimos);
			return $retorno;
		}
		if($maisVendidos){
			$banco = new BD();
			return $banco->mostra_registros($this->maisVendidos);
		}		
		$banco = new BD();
		$retorno = $banco->mostra_registros($this->exibicao);
		$banco->mostra_registros($this->exibicao);
		return $retorno;
	}
	
	function nomeAbreviado($qtdCarac='10'){
		if(strlen($this->nome)>$qtdCarac)	return substr($this->nome,0,($qtdCarac-3))."...";
		return $this->nome;
	}
	
	/*function venderProduto($cod_produto,$cod_empresa=''){
		$banco = new BD();
		if($cod_empresa)
		$baixa = "update ".TABELA_ESTOQUE." set ESTOQUE=ESTOQUE - 1 where COD_PRODUTO = '$cod_produto' and COD_EMPRESA = '$cod_empresa'";*/


/*	function alterarProduto()
	{
		$banco = new BD;	
		$altera = "update ".TABELA_PRODUTOS." set REFERENCIA='$this->referencia',MODELO='$this->modelo', NOME='$this->nome', 
		DESCRICAO='$this->descricao', COD_FABRICANTE='$this->cod_fabricante', 
		COD_TIPO_PRODUTO='$this->cod_tipo_produto', CODIGO_BARRAS='$this->codigo_barras', 
		DATA_CADASTRO='$this->data_cadastro', DESCONTINUADO='$this->descontinuado', 
		DATA_DESCONTINUADO='$this->data_descontinuado', PESO_LIQUIDO='$this->peso_liquido', 
		DATA_OPERACAO='$this->data_operacao', HORA_OPERACAO='$this->hora_operacao', 
		COD_ACESSO='$this->cod_acesso' where COD_PRODUTO = '$this->cod_produto'";
		if($banco->pesquisarBD($altera)){
			$banco->fechar();
			return true; // produto foi alterado
		}
		$banco->fechar();
		return false; // erro na altera��o do produto
	}
	
	function excluirProduto(){
		$banco = new BD;	
		$excluir = "delete from ".TABELA_PRODUTOS." where COD_PRODUTO='$this->cod_produto'";
		if($banco->pesquisarBD($excluir)){
			$banco->fechar();
			return true; // o produto foi excluido
		}
		$banco->fechar();
		return false; // erro na exclus�o do produto
	}
*/}
?>