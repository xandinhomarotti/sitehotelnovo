<?php 
include_once "BD.php";
class email { 
    var $settings     = array("erro"    => false, 
                              "from"    => true, 
                              "to"      => false, 
                              "subject" => false 
                        ); 
    var $encoding     = "8bit"; 
    var $text         = ""; 
    var $subject      = ""; 
    var $priority     = ""; 
    var $notification = ""; 
    var $format       = "yes"; 
    var $attachment   = array(); 
	var $attachment_name = array();
    var $inline       = array(); 
    var $text_mime    = array(); 
    var $from         = ""; 
    var $fromName     = ""; 
    var $to           = ""; 
    var $cc           = ""; 
    var $bcc          = ""; 
    var $replyto      = ""; 
    var $headers      = ""; 
    var $message      = ""; 
    var $x_mimeole    = "Produzido por Thiago Menezes"; 
    var $boundary     = "thiago@asmnet.com.br_"; 
    var $id           = "@asmnet.com.br"; 
    var $error        = false; 

//======================================================================== 
// Construtor 
//--------------------------------------- 
    function Email() { 
        
    } //======================================================================== 
// Define op��es para a classe de e-mail. 
//--------------------------------------- 
    function settings($erro = false, $from = true, $to = false, $subject = false) { 
        if ($erro == true) { 
            ini_set("track_errors","1"); 
        } 
        $this -> settings = array("erro"    => $erro, 
                                  "from"    => $from, 
                                  "to"      => $to, 
                                  "subject" => $subject 
                            ); 
    } 
//======================================================================== 
// Limpa o remetente e o destinatario 
//--------------------------------------- 
    function clear($erro = false, $from = true, $to = false, $subject = false) { 
        if ($erro == true) { 
            ini_set("track_errors","1"); 
        } 
        $this -> settings = array("erro"    => $erro, 
                                  "from"    => $from, 
                                  "to"      => $to, 
                                  "subject" => $subject 
                            ); 
    } 
//======================================================================== 
// Mostra mensagem de erro e para a execucao do script. 
//----------------------------------------------------- 
    function controle_erro($texto, $php_errormsg = "") { 
        $this -> error = true; 
        if ($this -> settings["erro"] == true) { 
            echo "<TABLE CellPadding='10' Border='1' BorderColor='#BBBBBB' CellSpacing='0'>\n<TR>\n<TD>\n<FONT Face='verdana' Size='1'>$texto"; 
            if ($php_errormsg <> "") { 
                echo "<BR><BR><I>" . $php_errormsg . "</I>"; 
            } 
            echo "</FONT>\n</TD>\n</TR>\n</TABLE>"; 
            exit; 
        } 
    } 
//======================================================================== 
// Transformar pagina html em mensagem de email. 
//----------------------------------------------------- 
	function getArquivo($arquivo_de_email)
	{
		#########################################
		# Leitura do arquivo de layout do email #
		#########################################		
		$filename = "$arquivo_de_email"; //indica o endere�o do arquivo que contem o layout
		$handle = fopen ($filename, "r"); // abre o arquivo que contem o layout
		$html = fread ($handle, filesize ($filename)); // l� o c�digo do arquivo e guarda na vari�vel $html 
		fclose ($handle); // Fecha o arquivo
		return $html;
	}
//======================================================================== 
// Define se o e-mail tera formato Mime ou nao. Default: Mime. 
//------------------------------------------------------------ 
    function set_format_mime($format = "yes") { 
        // no  = sem tipo Mime 
        // yes = com tipo Mime 
        $format = strtolower($format); 
        if ($format == "yes" OR $format == "no") { 
            $this -> format = $format; 
        } else { 
            $this -> controle_erro("A fun&ccedil;&atilde;o set_format_mime() s&oacute; aceita 'yes' ou 'no' como par&acirc;metro."); 
        } 
    } 
//======================================================================== 
// Define o nivel de prioridade. 
//------------------------------ 
    function set_priority($prioridade) { 
        // 1 = prioridade alta 
        // 3 = prioridade normal 
        // 5 = prioridade baixa 
        $prioridade = (int)$prioridade; 
        if (!in_array($prioridade,array(1,3,5))) {$this -> controle_erro("A fun&ccedil;&atilde;o set_priority() s&oacute; aceita os n&uacute;meros:<BR>1: para prioridade alta;<BR>3: para prioridade normal;<BR>5: para prioridade baixa.");} 
        switch ($prioridade) { 
            case 1: 
                $priority_tmp  = "X-Priority: 1\n"; 
                $priority_tmp .= "X-MSMail-Priority: High\n"; 
                break; 
            case 3: 
                $priority_tmp  = "X-Priority: 3\n"; 
                $priority_tmp .= "X-MSMail-Priority: Normal\n"; 
                break; 
            case 5: 
                $priority_tmp  = "X-Priority: 5\n"; 
                $priority_tmp .= "X-MSMail-Priority: Low\n"; 
                break; 
            default: $priority_tmp = ""; 
        } 
        $this -> priority = $priority_tmp; 
    } 
//======================================================================== 
// Define um pedido de confirmacao de leitura. 
//-------------------------------------------- 
    function set_notification($email, $nome = "") { 
        $email = trim($email); 
        if ($email == "") {$this -> controle_erro("E-mail n&atilde;o informado em set_notification().");} 
        $this -> notification = "Disposition-Notification-To: $nome <$email>\n"; 
    } 
//======================================================================== 
// Adiciona o remetente do e-mail. 
//-------------------------------- 
    function add_from($email, $nome = "") { 
        $email = trim($email); 
        if ($email == "") {$this -> controle_erro("E-mail n&atilde;o informado em add_from().");} 
        $this -> from = $email; 
        if ($nome <> "") { 
            $this -> fromName = "$nome <$email>"; 
        } 
    } 
//======================================================================== 
// Adiciona os destinatarios do e-mail. 
//------------------------------------- 
    function add_to($email, $nome = "", $restart = 0) { 
        $email = trim($email); 
        if ($email == "") {$this -> controle_erro("E-mail n&atilde;o informado em add_to().");} 
        if ($this -> to == "" OR $restart == 1) { 
            if ($nome <> "") { 
                $this -> to = "$nome <$email>"; 
            } else { 
                $this -> to = $email; 
            } 
        } else { 
            if ($nome <> "") { 
                $this -> to .= ", $nome <$email>"; 
            } else { 
                $this -> to .= ",$email"; 
            } 
        } 
    } 
//======================================================================== 
// Adiciona destinat�rios do Banco de dados. 
//----------------------------------------------------- 
    function destinatariosBD($tabela,$dest='') { 
        $banco = new BD();
		if($dest==''){
			$consulta = "select * from ".$tabela;
			$resultado = $banco->pesquisarBD($consulta);
			$dados = $banco->mostra_registros($resultado);
			while($dados){
				add_to($dados["email"],$dados["nome"]);
			}
		}
		else{
			$consulta = "select * from ".$tabela." where email=".$dest;
			$resultado = $banco->pesquisarBD($consulta);
			$dados = $banco->mostra_registros($resultado);
			add_to($dados["email"],$dados["nome"]);
		}
    } 
//======================================================================== 
// adiciona texto da mensagem
//------------------------------------------------------------ 
    function add_mensagem($msg) { 
		$texto_msg = $msg;
    } 
//======================================================================== 
// Adiciona os destinatarios Cc do e-mail. 
//---------------------------------------- 
    function add_cc($email, $nome = "", $restart = 0) { 
        $email = trim($email); 
        if ($email == "") {$this -> controle_erro("E-mail n&atilde;o informado em add_cc().");} 
        if ($this -> cc == "" OR $restart == 1) { 
            if ($nome <> "") { 
                $this -> cc = "$nome <$email>"; 
            } else { 
                $this -> cc = $email; 
            } 
        } else { 
            if ($nome <> "") { 
                $this -> cc .= ", $nome <$email>"; 
            } else { 
                $this -> cc .= ",$email"; 
            } 
        } 
    } 
//======================================================================== 
// Adiciona os destinatarios Bcc do e-mail. 
//----------------------------------------- 
    function add_bcc($email, $nome = "", $restart = 0) { 
        $email = trim($email); 
        if ($email == "") {$this -> controle_erro("E-mail n&atilde;o informado em add_bcc().");} 
        if ($this -> bcc == "" OR $restart == 1) { 
            if ($nome <> "") { 
                $this -> bcc = "$nome <$email>"; 
            } else { 
                $this -> bcc = $email; 
            } 
        } else { 
            if ($nome <> "") { 
                $this -> bcc .= ", $nome <$email>"; 
            } else { 
                $this -> bcc .= ",$email"; 
            } 
        } 
    } 
//======================================================================== 
// Adiciona o destinatario da resposta. 
//------------------------------------- 
    function add_replyto($email = "", $nome = "") { 
        $email = trim($email); 
        if ($email == "") {$email = $this -> from;} 
        $this -> replyto = "$nome <" . $email . ">"; 
    } 
//======================================================================== 
// Adiciona um texto ao email no formato Mime. 
//-------------------------------------------- 
    function add_text_mime($texto = "", $type = "text/html", $encoding = "8bit", $charset = "iso-8859-1") { 
        $text_tmp  = "Content-Type: $type;\n\tcharset=\"$charset\"\n"; 
        $text_tmp .= "Content-Transfer-Encoding: $encoding\n\n"; 
        $text_tmp .= "$texto\n\n"; 
        $this -> text_mime[] = $text_tmp;
    } 
//======================================================================== 
// Transforma um arquivo para a base64. 
//------------------------------------- 
    function build_base64($filename) { 
        if ($file = @fopen($filename, "r")) { 
            $contents = fread($file, filesize($filename)); 
            $encoded  = chunk_split(base64_encode($contents)); 
            fclose($file); 
            return $encoded; 
        } else { 
            $this -> controle_erro("N&atilde;o foi poss&iacute;vel ler o arquivo '$filename'.",$php_errormsg); 
        } 
    } 
//======================================================================== 
// Funcao que anexa um arquivo ao email. 
//-------------------------------------- 
    function add_attachment($filename, $name, $type="application/octet-stream") { 
        if (trim($filename) == "") {$this -> controle_erro("Endere&ccedil;o do arquivo n&atilde;o informado em add_attachment().");} 
        if (trim($name) == "")     {$this -> controle_erro("Nome do arquivo n&atilde;o informado em add_attachment().");} 
        $encoded = $this -> build_base64($filename); 
        $attachment_tmp  = "Content-Type: $type;\n\tname=\"$name\"\n"; 
        $attachment_tmp .= "Content-Transfer-Encoding: base64\n"; 
        $attachment_tmp .= "Content-Disposition: attachment;\n\tfilename=\"$name\"\n\n"; 
        $attachment_tmp .= "$encoded\n\n"; 
        $this -> attachment[] = $attachment_tmp;
		$this -> attachment_name[] = $name; 
    } 
//======================================================================== 
// Funcao que inclui um arquivo dentro do email. 
//---------------------------------------------- 
    function add_inline($filename, $name, $type="application/octet-stream") { 
        if (trim($filename) == "") {$this -> controle_erro("Endere&ccedil;o do arquivo n&atilde;o informado em add_inline().");} 
        if (trim($name) == "")     {$this -> controle_erro("Nome do arquivo n&atilde;o informado em add_inline().");} 
        $encoded     = $this -> build_base64($filename); 
        $cid         = md5(uniqid(time())) . $this -> id; 
        $inline_tmp  = "Content-Type: $type;\n\tname=\"$name\"\n"; 
        $inline_tmp .= "Content-ID: <$cid>\n"; 
        $inline_tmp .= "Content-Transfer-Encoding: base64\n\n"; 
        $inline_tmp .= "$encoded\n\n"; 
        $this -> inline[] = $inline_tmp; 
        return "cid:" . $cid; 
    } 
//======================================================================== 
// Funcao que monta o cabecalho de acordo com os dados e opcoes escolhidos. 
//------------------------------------------------------------------------- 
    function build_header() { 
        $mensagem = $header = ""; 
        // Cabecalho comum a todos os emails. 
        $this->headers .= ($this -> from <> "" AND $this -> settings["from"]) ? "From: "     . $this -> fromName  . "\n" : ""; 
        $this->headers .= ($this -> to <> "" AND $this -> settings["to"])     ? "To: "       . $this -> to        . "\n" : ""; 
        $this->headers .= ($this -> cc <> "")                                 ? "Cc: "       . $this -> cc        . "\n" : ""; 
        $this->headers .= ($this -> bcc <> "")                                ? "Bcc: "      . $this -> bcc       . "\n" : ""; 
        $this->headers .= ($this -> replyto <> "")                            ? "Reply-To: " . $this -> replyto   . "\n" : ""; 
        $this->headers .= ($this -> settings["subject"])                      ? "Subject: "  . $this -> subject   . "\n" : ""; 
        $this->headers .= "Date: " . date("r") . "\n"; 
        // Se o email nao for no formato MIME, somente envia o texto sem formatacao. Caso contrario, monta o cabecalho no formato MIME. 
        if ($this -> format == "no") { 
            $mensagem .= $this -> text; 
        } else { 
            // Define o cabecalho do email caso este for no formato MIME. 
            $this->headers .= "MIME-Version: 1.0\n"; 
            // Verifica se existe algum arquivo anexo, caso sim, tipo sera mixed, caso contrario, sera alternative 
            if (count($this -> attachment) > 0 OR count($this -> inline) > 0) { 
                $this->headers .= "Content-Type: multipart/mixed;\n\tboundary=\"" . $this -> boundary . "_00\"\n"; 
            } else { 
                $this->headers .= "Content-Type: multipart/alternative;\n\tboundary=\"" . $this -> boundary . "_00\"\n"; 
            } 
            $this->headers .= $this -> priority; 
            $this->headers .= "X-Mailer: PHP ". phpversion() ."\n"; 
            $this->headers .= $this -> notification; 
            $this->headers .= "X-MimeOLE: " . $this -> x_mimeole . "\n"; 
            $this->headers .= "Content-Transfer-Encoding: " . $this -> encoding . "\n\n"; 
            // Define o corpo do email 
            $mensagem  = "Esta mensagem esta no formato MIME. Seu leitor de email nao suporta este formato, por isso uma parte ou toda esta mensagem pode nao ser legivel.\n\n"; 
            if (count($this -> inline) > 0) { 
                $mensagem .= "--" . $this -> boundary . "_00\n"; 
                $mensagem .= "Content-Type: multipart/related;\n\tboundary=\"" . $this -> boundary . "_01\"\n\n\n"; 
                $mensagem .= "--" . $this -> boundary . "_01\n"; 
                $mensagem .= "Content-Type: multipart/alternative;\n\tboundary=\"" . $this -> boundary . "_02\"\n\n\n"; 
                for ($i = 0; $i < count($this -> text_mime) ; $i++) { 
                    $mensagem .= "--" . $this -> boundary . "_02\n"; 
                    $mensagem .= $this -> text_mime[$i]; 
                } 
                $mensagem .= "--" . $this -> boundary . "_02--\n\n"; 
                for ($i = 0; $i < count($this -> inline) ; $i++) { 
                    $mensagem .= "--" . $this -> boundary . "_01\n"; 
                    $mensagem .= $this -> inline[$i]; 
                } 
                $mensagem .= "--" . $this -> boundary . "_01--\n\n"; 
                for ($i = 0; $i < count($this -> attachment) ; $i++) { 
                    $mensagem .= "--" . $this -> boundary . "_00\n"; 
                    $mensagem .= $this -> attachment[$i]; 
                } 
            } elseif (count($this -> attachment) > 0) { 
                $mensagem .= "--" . $this -> boundary . "_00\n"; 
                $mensagem .= "Content-Type: multipart/alternative;\n\tboundary=\"" . $this -> boundary . "_01\"\n\n\n"; 
                for ($i = 0; $i < count($this -> text_mime) ; $i++) { 
                    $mensagem .= "--" . $this -> boundary . "_01\n"; 
                    $mensagem .= $this -> text_mime[$i]; 
                } 
                $mensagem .= "--" . $this -> boundary . "_01--\n\n"; 
                for ($i = 0; $i < count($this -> attachment) ; $i++) { 
                    $mensagem .= "--" . $this -> boundary . "_00\n"; 
                    $mensagem .= $this -> attachment[$i]; 
                } 
            } else { 
                for ($i = 0; $i < count($this -> text_mime) ; $i++) { 
                    $mensagem .= "--" . $this -> boundary . "_00\n"; 
                    $mensagem .= $this -> text_mime[$i]; 
                } 
            } 
            $mensagem .= "--" . $this -> boundary . "_00--\n\n"; 
        } 
        $this -> headers = $this->headers; 
        $this -> message = $mensagem; 
    } 
//======================================================================== 
// Funcao responsavel por enviar o e-mail. 
//---------------------------------------- 
    function send() { 
        $this -> build_header(); 
        if ($this -> error == false) { 
            $enviou = @mail($this -> to, $this -> subject, $this -> message, $this -> headers); 
            if (!$enviou) { 
                $this -> controle_erro("N�o enviou", isset($php_errormsg) ? $php_errormsg : ""); 
            } 
        } else { 
            $enviou = false; 
        } 
        return $enviou;
    } 
//======================================================================== 
} // Fim da classe Email. 
?>