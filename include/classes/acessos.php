<?php
include_once "include/funcoesUteis.php";
include_once "include/classes/BD.php";
//_________________________________________________________________________________________________
// definindo as tabelas relacionadas a acesso
//_________________________________________________________________________________________________
define('TABELA_CLIENTES', 'clientes');
define('TABELA_ADMIN','administradores');
define('TABELA_ACESSOS','acessos');

class acesso{

	var $id_acesso;
	var $hora_acesso;
	var $data_acesso;
	var $hora_saida;	
	var $ip_acesso;
	var $computador_acesso;
	var $navegador_acesso;
	var $id_usuario_acesso;
	var $login_acesso;
	var $senha_acesso;
	var $banco;
	var $sql;
			
	function acesso($id_acesso='',$id_usuario='',$login=''){
		$this->banco = new BD;
		$consulta = "select * from ".TABELA_ACESSOS." where COD_ACESSO='$id_acesso' and
		COD_USUARIO='id_usuario' and LOGIN='$login'";
		$resultado = $this->banco->pesquisarBD($consulta);
		$dados = $this->banco->mostra_registros($resultado);
		if($dados){
			$this->id_acesso = $dados["COD_ACESSO"];
			$this->hora_acesso = $dados["HORA_OPERACAO"];
			$this->data_acesso = $dados["DATA_OPERACAO"];
			$this->hora_saida = $dados["HORA_SAIDA"];			
			$this->ip_acesso = $dados["IP"];
			$this->computador_acesso = $dados["COMPUTADOR"];
			$this->navegador_acesso = $dados["NAVEGADOR"];
			$this->id_usuario_acesso = $dados["COD_USUARIO"];
			$this->login_acesso = $dados["LOGIN"];
			$this->senha_acesso = $dados["SENHA"];
		}
		$this->id_acesso = $id_acesso;
		$this->id_usuario_acesso = $id_usuario;
		$this->login_acesso = $login;		
	}
	
	function iniciaAcesso($cod_usuario,$login,$senha,$pc,$ip,$navegador){
		$this->computador_acesso = $pc;
		$this->ip_acesso = $ip;
		$this->navegador_acesso = $navegador;	
		$this->id_usuario_acesso = $cod_usuario;
		$this->login_acesso = $login;
		$this->senha_acesso = $senha;
		$this->hora_acesso = date('H:i:s');
		$this->data_acesso = date('Y-m-d');
		$banco = new BD();
		$insere = "insert into ".TABELA_ACESSOS." 
		(HORA_OPERACAO, DATA_OPERACAO, HORA_SAIDA, IP, COMPUTADOR, NAVEGADOR, COD_USUARIO, 		
		LOGIN, SENHA)
		values
		('$this->hora_acesso', '$this->data_acesso', '$this->hora_saida', '$this->ip_acesso', 
		'$this->computador_acesso', '$this->navegador_acesso', '$this->id_usuario_acesso', '$this->login_acesso', password('$this->senha_acesso'))";
		$this->sql = $insere;
		if($banco->pesquisarBD($insere)){
			$this->id_acesso = novoCodigo(TABELA_ACESSOS,"COD_ACESSO");
			return true; // acesso foi cadastrado
		}
		return false; // erro no cadastro do acesso		
	}

	function cadastrarAcesso()
	{
		$banco = new BD();
		$insere = "update ".TABELA_ACESSOS." set HORA_SAIDA='$this->hora_saida' where COD_ACESSO='$this->id_acesso'";
		$this->sql = $insere;
		if($banco->pesquisarBD($insere)){
			return $insere; // acesso foi cadastrado
		}
		return false; // erro no cadastro do acesso
	}
	
	function finalizaAcesso(){
		$this->hora_saida = date('H:i:s');
		$this->computador_acesso = $pc;
		$this->ip_acesso = $ip;
		$this->navegador_acesso = $navegador;
		setcookie("logado", "", time()-3600);
		if($this->cadastrarAcesso()) return true;
		return false;
	}
	
	function logon($login,$senha,$tipo_usuario,$pc,$ip,$navegador){
		$this->login_acesso = $login;
		$this->senha_acesso = $senha;
		switch($tipo_usuario){
			case 'C': $pesquisa = "select * from ".TABELA_CLIENTES." where EMAIL='$this->login_acesso' and FINALIZADO='S'";
					  $pesquisa2 = "select * from ".TABELA_CLIENTES." where SENHA=password('$this->senha_acesso') and EMAIL='$this->login_acesso' and FINALIZADO='S'";
					  $pesquisa3 = "select * from ".TABELA_CLIENTES." where EMAIL='$this->login_acesso' and FINALIZADO='N'";
					  $cod_usuario = 'COD_CLIENTE';
					  break;
			case 'A': $pesquisa = "select * from ".TABELA_ADMIN." where EMAIL='$this->login_acesso'";
					  $pesquisa2 = "select * from ".TABELA_ADMIN." where SENHA=password('$this->senha_acesso') and EMAIL='$this->login_acesso'";
					  $cod_usuario = 'COD_ADMIN';							
					  break;
			default : return "tipo de usu�rio n�o fornecido como par�metro";
		}
		$resultado=$this->banco->pesquisarBD($pesquisa);
		$resultado2=$this->banco->pesquisarBD($pesquisa2);	
		$resultado3=$this->banco->pesquisarBD($pesquisa3);	
		$num=$this->banco->total_registros($resultado);
		$num2=$this->banco->total_registros($resultado2);
		$num3=$this->banco->total_registros($resultado3);
		$dados = $this->banco->mostra_registros($resultado2);
		if($num!=0){
			if($num2!=0){
				$this->iniciaAcesso($dados[$cod_usuario],$this->login_acesso,$this->senha_acesso,$pc,$ip,$navegador);
				setcookie("login", $this->login_acesso,time()+60*60*24*30);
				setcookie("senha", $this->senha_acesso,time()+60*60*24*30);
				setcookie("tipo_usuario", $tipo_usuario,time()+60*60*24*30);
				setcookie("logado", true,time()+60*60*24*30);							
				if($dados["TIPO_CLIENTE"]=='F') return 'F'; // login e senha corretos FISICO
				else if($dados["TIPO_CLIENTE"]=='J') return 'J'; // login e senha corretos JURIDICO
				else return 'A';
			}
			else	return 2;  // login correto e senha errada
		}
		if($num3!=0) return 4; // est� cadastrado mas ainda n�o confirmo
		return 3; // usu�rio n�o cadastrado
	}
}
?>