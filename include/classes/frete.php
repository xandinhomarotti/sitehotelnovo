<? 
/* 
+-----------------------------------------------------------------------+ 
| CalcFrete v0.1                                                        | 
+-----------------------------------------------------------------------+ 
| Data: 28/08/2003 - 22:38                                              | 
| Copyright (c) 2003                                                    | 
+-----------------------------------------------------------------------+ 
| Script de c�lculo de frente junto aos correios                        | 
| 40010 - SEDEX Convencional  											|
| 40045 - SEDEX � Cobrar												|			 
| 40215 - SEDEX 10                                                      | 
| 40290 - SEDEX Hoje                                          			|			 
+-----------------------------------------------------------------------+ 
| Autor: Rafael Verdi Sachetto <http://www.labstyle.com.br>             | 
+-----------------------------------------------------------------------+ 
| Exemplo:																|
|$Teste = new Frete("40045","88820-000","86030-400",30,"","",150,47);	|
|if($Teste->calcular()){												|
|	for($i=0;$i<count($Teste->Resposta);$i++){							|
|		echo $Teste->Resposta[$i].' -- ';								|
|	}	            			        								|
|}																		|
|else{																	|
|	echo "<Script>alert('".$Teste->getErro()."')</Script>";				|
|}																		|
+-----------------------------------------------------------------------+ 
| Modifica��o 0.4 by Thiago Piazza         01-09-2006					|
|	- Add do atributo PesoMaximo										|
|	- aperfei�oamento do script para calcular pesos acima do maximo		|
+-----------------------------------------------------------------------+ 
*/ 
class Frete{ 
	// Atributos
	    var $TipoServico; 							// 40010 - SEDEX Convencional | 40045 - SEDEX � Cobrar | 40215 - SEDEX 10 | 40290 - SEDEX Hoje
	    var $CEPOrigem;								//
	    var $CEPDestino;							//
	    var $Peso; 									// maximo de 30Kg
	    var $Mostra=true;							// mostrar menssagem de erro 	    
		var $MaoPropria;							// Recebimento em M�os Pr�prias - s ou n
		var $Aviso;									// Aviso de Recebimento - s ou n
		var $Valor;									// Valor Declarado
		var $Resposta=Array("","","",0,0,0,0);		// Array[] || �ndices: 	0 - Nome do Servi�o
													//						1 - Nome de Origem
													//						2 - Nome de Destino
													//						3 - Valor do Servi�o Aviso de Recebimento
													//						4 - Valor do Servi�o M�os Pr�prias
													//						5 - Valor da Tarifa cobrada em cima do Valor Declarado
													//						6 - Valor Total do Servi�o
		var $Erro=false;							// Vari�vel de Tratamento de Erros
		var $PesoMaximo=30;							// V�riavel que identifica o peso m�ximo permitido pelo correio para calcular valores acima deste peso
    //
    
    //Contrutor
    	function Frete($TipoServico,$CEPOrigem,$CEPDestino,$Peso,$MaoPropria,$Aviso,$Valor){
	    	$this->setTipoServico($TipoServico);	
	    	$this->setCEPOrigem($CEPOrigem);	
	    	$this->setCEPDestino($CEPDestino);	
	    	$this->setPeso($Peso);		    		    		    	
	    	$this->setMaoPropria($MaoPropria);		    		    		    	
	    	$this->setAviso($Aviso);		    		    		    	
	    	$this->setValor($Valor);		    		    		    		    		    		    	
	    }
    //
    
    //Metodos
	    function calcular(){ 
		    
		    if($this->getPeso()>$this->getPesoMaximo()){
			    $PesoTemp=$this->getPesoMaximo();
				$ValorMultiplicado=($this->getPeso()/$this->getPesoMaximo());    
			}
			else{
			    $PesoTemp=$this->getPeso();				
				$ValorMultiplicado=1;
			}
			
       		$ArquivoCorreio = "http://www.correios.com.br/encomendas/precos/calculo.cfm?servico=" . $this->getTipoServico() . "&CepOrigem=".$this->getCEPOrigem()."&CepDestino=".$this->getCEPDestino()."&Peso=".$PesoTemp."&MaoPropria=".$this->getMaoPropria()."&valorDeclarado=".$this->getValor()."&avisoRecebimento=".$this->getAviso();
			ini_set("allow_url_fopen", 1); 
	        if($Conteudo = @join("",file($ArquivoCorreio))){ 
				if($this->pegaDados($Conteudo,$ValorMultiplicado)){
					return true;	
				}
				else{
					return false;	
				}
			}
			else{
				$this->setErro("Servi�o indispon�vel no momento. Favor tentar novammente mais tarde!");
	            return false; 
			}
		}
	    
	    function pegaDados($Conteudo,$ValorMultiplicado){
   	            $Dados = Array('Servico=','UFOrigem=','LocalOrigem=','UFdestino=','LocalDestino=','MaoPropria=','AvisoRecebimento=','valorDeclarado=','Tarifa=','erro=');
   	            $Fins = Array('&cepOrigem=','&LocalOrigem','&UFdestino=','&LocalDestino=','&Peso=','&AvisoRecebimento=','&valorDeclarado=','&Tarifa=','&erro=',"\",\"popUpTarifa\",\"scrollbars=no,status=no,width=380,height=320\"");   	            
		        $Resultado = Array(count($Dados));
		        
		        for($i=0;$i<count($Dados);$i++){
					$Procura = strpos($Conteudo,$Dados[$i])+strlen($Dados[$i]); 
					$Conteudo = trim(substr($Conteudo,$Procura)); 
		            $Fim = strpos($Conteudo,$Fins[$i]); 
		            $Resultado[$i]=trim(substr($Conteudo,0,$Fim)); 
	            }
	            
	            //limpando vari�veis j� utilizadas
		            unset($Procura);
		            unset($Conteudo);
		            unset($Fim);
		            unset($Fins);	        
		            unset($Dados);	        		            
	            //    	            	            
				
	            // Inserindo valor no atributo array $Resposta
	            	$Resposta[0]=$this->limpaChars($Resultado[0]);						// Nome do Servico
	            	$Resposta[1]=$Resultado[1].' - '.$this->limpaChars($Resultado[2]);	// Nome de Origem
	            	$Resposta[2]=$Resultado[3].' - '.$this->limpaChars($Resultado[4]);	// Nome de Destino	            	
	            	$Resposta[3]=$Resultado[5];											// Valor do Servi�o Aviso de Recebimento
	            	$Resposta[4]=$Resultado[6];											// Valor do Servi�o M�os Pr�pria
	            	$Resposta[5]=$Resultado[7];											// Valor da Tarifa cobrada em cima do Valor Declarado
	            	$Resposta[6]=($Resultado[8]*$ValorMultiplicado);					// Valor Total do Servi�o		        
	            	$Erro = $this->limpaChars($Resultado[9]);							// Vazio = Sucesso, Preenchido = Falhou a Opera��o
	            	
		            //limpando vari�veis j� utilizadas
			            unset($Resultado);
		            //    	            	            	            	
	            	
	            	if(!empty($Erro)){
		            	$this->setErro($Erro);
	  		            //limpando vari�veis j� utilizadas
				            unset($Erro);
			            //    	            	            	            			            	
	            	}
	            //
	            
	            if($this->getErro()){
		        	return false;    
		        }
		        else{
		            $this->Resposta=$Resposta;	            					        

  		            //limpando vari�veis j� utilizadas
			            unset($Resposta);
		            //    	            	            	            	
					return true;
			    }
		}
		
		function limpaChars($Valor){
	   		
			$Valor=str_replace('%E7o','dor',$Valor);		// Substitui %E7o por dor
			$Valor=str_replace('%20',' ',$Valor);			// Substitui %20 por espa�os
			$Valor=str_replace('%ED','�',$Valor);			// Substitui %ED por �
			$Valor=str_replace('%E3','�',$Valor);			// Substitui %E3 por �
			$Valor=str_replace('%E1','�',$Valor);			// Substitui %E1 por �	
			$Valor=str_replace('%F3','�',$Valor);			// Substitui %F3 por �				
			
			return $Valor;
		}
    //
    
    //SETS
    	function setTipoServico($Valor){
	    	$this->TipoServico=$Valor;	
	    }
    	function setCEPOrigem($Valor){
	    	$this->CEPOrigem=$Valor;	
	    }
    	function setCEPDestino($Valor){
	    	$this->CEPDestino=$Valor;	
	    }
    	function setPeso($Valor){
	    	$this->Peso=$Valor;	
	    }
    	function setMaoPropria($Valor){
	    	$this->MaoPropria=$Valor;	
	    }	    	    	    	    
    	function setAviso($Valor){
	    	$this->Aviso=$Valor;	
	    }	    	    	    	    
    	function setValor($Valor){
	    	$this->Valor=$Valor;	
	    }	    	    	    	    	    	    	    
    	function setErro($Valor){
	    	$this->Erro=$Valor;	
	    }	    	    	    	    	    	    	    
    	function setPesoMaximo($Valor){
	    	$this->PesoMaximo=$Valor;	
	    }	    	    	    	    	    	    	    	    	    
    	function setMostra($Valor){
	    	$this->Mostra=$Valor;	
	    }	    	    	    	    
    //
    
    //GETS
    	function getTipoServico(){
	    	return $this->TipoServico;	
	    }
    	function getCEPOrigem(){
	    	return $this->CEPOrigem;	
	    }
    	function getCEPDestino(){
	    	return $this->CEPDestino;	
	    }
    	function getPeso(){
	    	return $this->Peso;	
	    }
    	function getMaoPropria(){
	    	return $this->MaoPropria;	
	    }	    	    	    	    
    	function getAviso(){
	    	return $this->Aviso;	
	    }	    	    	    	    
    	function getValor(){
	    	return $this->Valor;	
	    }	    	    	    	
    	function getErro(){
	    	return $this->Erro;	
	    }	    	    	    	    	    	    	    	        
    	function getPesoMaximo(){
	    	return $this->PesoMaximo;	
	    }	    	    	    	    	    	    	    	        	    
    	function getMostra(){
	    	return $this->Mostra;	
	    }	    	    	    	    	    	    	    
    //
    
    //TO STRING
		function toString(){
			$Retorno="TipoServico:".$this->getTipoServico()."\CEP de Origem:".$this->getCEPOrigem()."\nCEP de Destino:".$this->getCEPDestino()."\nPeso:".$this->getPeso()."\nMaoPropria:".$this->getMaoPropria()."\nAviso:".$this->getAviso()."\nValor:".$this->getValor()."\nPesoMaximo:".$this->getPesoMaximo()."\nMostra:".$this->getMostra();
			return $Retorno;
		}
    //
} 
?> 