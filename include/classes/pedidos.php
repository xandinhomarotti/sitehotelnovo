<?php
include_once "include/funcoesUteis.php";
include_once "include/classes/BD.php";
include_once "include/classes/clientes.php";
include_once "include/classes/produtos.php";
include_once "include/classes/tiposPagamento.php";
//_________________________________________________________________________________________________
// definindo as tabelas relacionadas a pedidos
//_________________________________________________________________________________________________
define('TABELA_PEDIDOS', 'pedidos');
define('TABELA_PAGAMENTOS','pagamentos');
define('TABELA_ENDERECOS_ENTREGA','enderecos_entrega');
define('TABELA_CLIENTES', 'clientes');
define('TABELA_CLIENTES_PJ','clientes_pj');
define('TABELA_CLIENTES_PF','clientes_pf');
define('TABELA_ACESSOS','acessos');
define('TABELA_PRODUTOS_PEDIDO','produtos_pedido');

/*---------------------------------------------------------------------------------------------------
Classe pedidos
---------------------------------------------------------------------------------------------------*/
class pedido{
	
	var $cod_pedido; 
	var $cod_cliente;
	var $data_pedido;
	var $status;
	var $status_venda;
	var $cod_endereco_entrega;
	var $telefone_contato;
	var $celular_contato;
	var $nome_contato;
	var $tipo_endereco;
	var $logradouro;
	var $endereco;
	var $numero;
	var $complemento;
	var $referencia;
	var $cep;
	var $bairro;
	var $cidade;
	var $estado;
	var $observacoes;
	var $total_pedido;
	var $valor_frete;
	var $peso_pedido;
	var $cod_pagamento;
	var $tipo_pagamento;
	var $cod_moeda;
	var $data_operacao;
	var $hora_operacao;
	var $cod_acesso;
	var $cliente;
	var $sql;
	var $produtos = array("cod_produto"=>array(),"qtd"=>array(),"valor"=>array(),"presente"=>array());

	function pedido($cod_pedido=''){
		$banco = new BD;
		$consulta = "select * from ".TABELA_PEDIDOS." where COD_PEDIDO='$cod_pedido'";
		$this->sql = $consulta;
		$resultado = $banco->pesquisarBD($consulta);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$this->cod_pedido = $dados["COD_PEDIDO"];
			$this->cod_cliente = $dados["COD_CLIENTE"];
			$this->data_pedido = $dados["DATA_PEDIDO"];
			$this->status_venda = $dados["STATUS_VENDA"];
			$this->status_pedido = $dados["STATUS_PEDIDO"];
			$this->cod_endereco_entrega = $dados["COD_ENDERECO_ENTREGA"];
			$this->observacoes = $dados["OBSERVACOES"];
			$this->total_pedido = $dados["TOTAL_PEDIDO"];
			$this->valor_frete = $dados["VALOR_FRETE"];
			$this->peso_pedido = $dados["PESO_PEDIDO"];
			$this->cod_pagamento = $dados["COD_PAGAMENTO"];
			$this->tipo_pagamento = $dados["TIPO_PAGAMENTO"];			
			$this->cod_moeda = $dados["COD_MOEDA"];
			$this->data_operacao = $dados["DATA_OPERACAO"];
			$this->hora_operacao = $dados["HORA_OPERACAO"];
			$this->cod_acesso = $dados["COD_ACESSO"];			
		}
		$consulta = "select * from ".TABELA_PRODUTOS_PEDIDO." where COD_PEDIDO='$this->cod_pedido'";
		$this->sql = $consulta;
		$resultado = $banco->pesquisarBD($consulta);
		$cont=0;
		while($dados = $banco->mostra_registros($resultado)){
			$this->produtos["cod_produto"][$cont] = $dados["COD_PRODUTO"];
			$this->produtos["qtd"][$cont] = $dados["QUANTIDADE"];
			$this->produtos["presente"][$cont] = $dados["PRESENTE"];
			$this->produtos["valor"][$cont] = $dados["VALOR_TOTAL_PRODUTO"]/$dados["QUANTIDADE"];
			$cont++;
		}
		$banco = new BD;
		$consulta = "select * from ".TABELA_ENDERECOS_ENTREGA." where COD_ENDERECO_ENTREGA='$this->cod_endereco_entrega'";
		$this->sql = $consulta;
		$resultado = $banco->pesquisarBD($consulta);
		$dados = $banco->mostra_registros($resultado);
		$this->telefone_contato = $dados["TELEFONE_CONTATO"];
		$this->celular_contato = $dados["CELULAR_CONTATO"];
		$this->nome_contato = $dados["NOME_CONTATO"];
		$this->tipo_endereco = $dados["TIPO_ENDERECO"];
		$this->logradouro = $dados["LOGRADOURO"];
		$this->endereco = $dados["ENDERECO"];
		$this->numero = $dados["NUMERO"];
		$this->complemento = $dados["COMPLEMENTO"];
		$this->referencia = $dados["REFERENCIA"];
		$this->cep = $dados["CEP"];
		$this->bairro = $dados["BAIRRO"];
		$this->cidade = $dados["CIDADE"];
		$this->estado = $dados["ESTADO"];
	}
	
	function incluirProduto($cod_produto,$qtd = 1,$presente = 'N'){
		$cont=0;
		while($this->produtos["cod_produto"][$cont]){
			if($this->produtos["cod_produto"][$cont]==$cod_produto)	return false;
			$cont++;
		}
		$novoItem = new produto('',$cod_produto);
		array_push($this->produtos["cod_produto"],$cod_produto);
		array_push($this->produtos["qtd"],$qtd);
		$total_item = $novoItem->valor*$qtd;
		array_push($this->produtos["valor"],$total_item);
		array_push($this->produtos["presente"],$presente);
		$this->total_pedido+=$total_item;
		$this->calcularPeso();
		setcookie("produtos_pedido", $cod_produto,time()+60*60*24*30);
		return true;
	}
	
	function removerItem($cod){
		$chave = array_search($cod,$this->produtos["cod_produto"]);
		$total_item = $this->produtos["valor"][$chave]*$this->produtos["qtd"][$chave];
		$this->total_pedido-=$total_item;
		$this->produtos["cod_produto"]=diminuiArray($this->produtos["cod_produto"],$this->produtos["cod_produto"][$chave]);
		$this->produtos["qtd"]=diminuiArray($this->produtos["qtd"],$this->produtos["qtd"][$chave]);
		$this->produtos["valor"]=diminuiArray($this->produtos["valor"],$this->produtos["valor"][$chave]);
		$this->produtos["presente"]=diminuiArray($this->produtos["presente"],$this->produtos["presente"][$chave]);
		$this->calcularPeso();
		setcookie("produtos_pedido", $this->produtos,time()+60*60*24*30);
		return true;
	}
	
	function itemPresente($cod){
		$chave = array_search($cod,$this->produtos["cod_produto"]);
		if($this->produtos["presente"][$chave]=='S')	$this->produtos["presente"][$chave] = 'N';
		else	$this->produtos["presente"][$chave] = 'S';
		setcookie("produtos_pedido", $this->produtos,time()+60*60*24*30);
		return true;
	}
	
	function alterarQtdItem($cod,$qtd){
		$chave = array_search($cod,$this->produtos["cod_produto"]);
		$total_item = $this->produtos["valor"][$chave]*$this->produtos["qtd"][$chave];
		$this->total_pedido-=$total_item;
		$this->produtos["qtd"][$chave] = $qtd;
		$total_item = $this->produtos["valor"][$chave]*$this->produtos["qtd"][$chave];
		$this->total_pedido+=$total_item;
		$this->calcularPeso();
		setcookie("produtos_pedido", $this->produtos,time()+60*60*24*30);
		return true;
	}	
	
	function exibirProdutos($cont){
		return $this->produtos["cod_produto"][$cont];
	}
	
	function valorItem($indice){
		return exibirValor($this->produtos["valor"][$indice]);
	}
	
	function valorPedido(){
		return exibirValor($this->total_pedido+$this->valor_frete);
	}

	function calcularPeso(){
		$this->peso_pedido=0.000;
		$cont=0;
		while($this->produtos["cod_produto"][$cont]){
			$peso = new produto('',$this->produtos["cod_produto"][$cont]);
			$this->peso_pedido += $this->produtos["qtd"][$cont]*$peso->peso_liquido;
			$cont++;
		}
	}
	
	function finalizarPedido($log){
		$banco = new BD();
		$this->data_operacao = converteDataUsuario(date('d/m/Y'));
		$this->hora_operacao = date('H:i:s');
		$this->cod_acesso = $log->id_acesso;
		$this->cod_cliente = $log->id_usuario_acesso;
		$insere_endereco_entrega = "insert into ".TABELA_ENDERECOS_ENTREGA."(TELEFONE_CONTATO, CELULAR_CONTATO, NOME_CONTATO, TIPO_ENDERECO, LOGRADOURO, ENDERECO, NUMERO, COMPLEMENTO, REFERENCIA, CEP, BAIRRO, CIDADE, ESTADO, DATA_OPERACAO, HORA_OPERACAO, COD_ACESSO)values('$this->telefone_contato', '$this->celular_contato', '$this->nome_contato', '$this->tipo_endereco', '$this->logradouro', '$this->endereco', '$this->numero', '$this->complemento', '$this->referencia', '$this->cep', '$this->bairro', '$this->cidade', '$this->estado', '$this->data_operacao', '$this->hora_operacao', '$this->cod_acesso')";
		if($banco->pesquisarBD($insere_endereco_entrega)){
			$this->cod_endereco_entrega = novoCodigo(TABELA_ENDERECOS_ENTREGA,"COD_ENDERECO_ENTREGA");
			$insere_pedido = "insert into ".TABELA_PEDIDOS."(COD_CLIENTE, COD_ENDERECO_ENTREGA, DATA_PEDIDO, STATUS_VENDA, STATUS_PEDIDO, OBSERVACOES, TOTAL_PEDIDO,VALOR_FRETE, PESO_PEDIDO, COD_PAGAMENTO, COD_MOEDA, DATA_OPERACAO, HORA_OPERACAO, COD_ACESSO)values('$this->cod_cliente', '$this->cod_endereco_entrega', '$this->data_operacao', 'P', 'P', '$this->observacoes', '$this->total_pedido', '$this->valor_frete', '$this->peso_pedido', '$this->cod_pagamento', '1', '$this->data_operacao', '$this->hora_operacao', '$this->cod_acesso')";
			if($banco->pesquisarBD($insere_pedido)){
				$this->cod_pedido = novoCodigo(TABELA_PEDIDOS,"COD_PEDIDO");
				$cont=0;
				while($this->produtos["cod_produto"][$cont]){
					$itemVenda = new produto('',$this->produtos["cod_produto"][$cont]);
					$insere_produtos_pedido = "insert into ".TABELA_PRODUTOS_PEDIDO."(COD_PRODUTO, COD_PEDIDO, QUANTIDADE, COD_EMPRESA, VALOR_TOTAL_PRODUTO, PRESENTE, DATA_OPERACAO, HORA_OPERACAO, 	COD_ACESSO)values('$itemVenda->cod_produto', '$this->cod_pedido', '".$this->produtos["qtd"][$cont]."', '$itemVenda->cod_empresa', '".$this->produtos["valor"][$cont]*$this->produtos["qtd"][$cont]."', '".$this->produtos["presente"][$cont]."', '$this->data_operacao', '$this->hora_operacao', '$this->cod_acesso')";
					$banco = new BD();
					if(!($banco->pesquisarBD($insere_produtos_pedido))) return false;
					$cont++;
				}
				session_unregister("pedido");
				return $this->cod_pedido;
			}
			else return false;
		}
		else return false;
	}
}
?>