<?php
include_once "include/funcoesUteis.php";
include_once "include/classes/BD.php";
//_________________________________________________________________________________________________
// definindo as tabelas relacionadas a clientes
//_________________________________________________________________________________________________
define('TABELA_CLIENTES', 'clientes');
define('TABELA_CLIENTES_PJ','clientes_pj');
define('TABELA_CLIENTES_PF','clientes_pf');

class cliente{

	var $id_cliente;
	var $tel1_cliente;
	var $tel2_cliente;
	var $email_cliente;
	var $senha_cliente;
	var $apelido_cliente;
	var $maladireta_cliente;
	var $tipo_endereco_cliente;
	var $rua_endereco_cliente;
	var $numero_endereco_cliente;
	var $complemento_endereco_cliente;
	var $referencia_endereco_cliente;
	var $cep_endereco_cliente;
	var $bairro_endereco_cliente;
	var $cidade_endereco_cliente;
	var $estado_endereco_cliente;
	var $tipo_cliente;
	var $banco;
	var $novaChave;
	var $finalizado = 'N';
		
	function cliente($email='',$senha=''){
		$this->banco = new BD;
		$consulta = "select * from ".TABELA_CLIENTES." where EMAIL='$email' and
		senha_cliente=password('$senha')";
		$resultado = $this->banco->pesquisarBD($consulta);
		$dados = $this->banco->mostra_registros($resultado);
		if($dados){
			$this->id_cliente = $dados["COD_CLIENTE"];
			$this->tel1_cliente = $dados["TELEFONE_RESIDENCIAL"];
			$this->tel2_cliente = $dados["TELEFONE_CELULAR"];
			$this->email_cliente = $dados["EMAIL"];
			$this->senha_cliente = $dados["SENHA"];
			$this->apelido_cliente = $dados["APELIDO"];
			$this->maladireta_cliente = $dados["MALADIRETA"];
			$this->tipo_endereco_cliente = $dados["TIPO_ENDERECO"];
			$this->rua_endereco_cliente = $dados["ENDERECO"];
			$this->numero_endereco_cliente = $dados["NUMERO"];
			$this->complemento_endereco_cliente = $dados["COMPLEMENTO"];
			$this->referencia_endereco_cliente = $dados["REFERENCIA"];
			$this->cep_endereco_cliente = $dados["CEP"];
			$this->bairro_endereco_cliente = $dados["BAIRRO"];
			$this->cidade_endereco_cliente = $dados["CIDADE"];
			$this->estado_endereco_cliente = $dados["ESTADO"];
			$this->tipo_cliente = $dados["TIPO_CLIENTE"];
			return true;
		}
		$this->email_cliente = $email;
		$this->senha_cliente = $senha;
		return false;
	}
	
	function enviarEmail(){
		$header  = "MIME-Version: 1.0\n";
		$header .= "From: {$this->headers['from']}\n";
		$semi_rand     = md5(time());
		$this->headers_formatado = $header;
		$mime_boundary = "boundary-example$semi_rand";
		$body .= "\n\n--$mime_boundary Content-Type: text/html; charset=\"iso-8859-1\";
Content-Transfer-Encoding: quoted-printable;\n\n"."<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />
<title>Altarede</title>
</head>

<body>
<table width=\"600\" border=\"1\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bordercolor=\"#CCCCCC\">
  <tr>
    <td bordercolor=\"#FFFFFF\" bgcolor=\"#FFFFFF\"><div align=\"center\"><a href=\"../index.php\"></a>
      <table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
        <tr>
          <td><img src=\"img/topo_email.gif\" width=\"600\" height=\"98\" /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><div align=\"center\">teste email </div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </div></td>
  </tr>
</table>
</body>
</html>";
$body .="--$mime_boundary--";
	if(mail($this->email,"Confirma��o de cadastro no site Altarede", $body, $headers_formatado )){
		return true;
	}
	else{
		return false;
	}
}
	
	function verificaCadastro($email){
		$verifica = "select * from ".TABELA_CLIENTES." where EMAIL = '$email'";
		$resultado = $this->banco->pesquisarBD($verifica);
		$dados = $this->banco->mostra_registros($resultado);
		if($dados){
			return true;
		}
		return false;
	}
	
	function cadastrarCliente()
	{
		$insere = "insert into ".TABELA_CLIENTES." 
		(TELEFONE_RESIDENCIAL, TELEFONE_CELULAR, EMAIL, 
		SENHA, APELIDO, MALADIRETA, 
		TIPO_ENDERECO, ENDERECO, 
		NUMERO, COMPLEMENTO, 
		REFERENCIA, CEP, 
		BAIRRO, CIDADE, 
		ESTADO, TIPO_CLIENTE, FINALIZADO)
		values
		('$this->tel1_cliente', '$this->tel2_cliente', '$this->email_cliente', 
		password('$this->senha_cliente'), '$this->apelido_cliente', '$this->maladireta_cliente', 
		'$this->tipo_endereco_cliente', '$this->rua_endereco_cliente', 
		'$this->numero_endereco_cliente', '$this->complemento_endereco_cliente', 
		'$this->referencia_endereco_cliente', '$this->cep_endereco_cliente', 
		'$this->bairro_endereco_cliente', '$this->cidade_endereco_cliente', 
		'$this->estado_endereco_cliente', '$this->tipo_cliente', '$this->finalizado')";
		if($this->banco->pesquisarBD($insere)){
			$this->novaChave = novoCodigo(TABELA_CLIENTES,"COD_CLIENTE");
			if($this->enviarEmail()){
				$this->banco->fechar();
				return true; // cliente foi cadastrado
			}
		}
		$this->banco->fechar();
		return false; // erro no cadastro do cliente
	}
		
	function alterarCliente()
	{
		$altera = "update ".TABELA_CLIENTES." set TELEFONE_RESIDENCIAL='$this->tel1_cliente',
		TELEFONE_CELULAR='$this->tel2_cliente', EMAIL='$this->email_cliente', 
		SENHA=password('$this->senha_cliente'), APELIDO='$this->apelido_cliente',
		MALADIRETA='$this->maladireta_cliente', 
		TIPO_ENDERECO='$this->tipo_endereco_cliente',
		ENDERECO='$this->	rua_endereco_cliente', 
		NUMERO='$this->numero_endereco_cliente', 	
		COMPLEMENTO='$this->complemento_endereco_cliente', 
		REFERENCIA='$this->referencia_endereco_cliente', 	
		CEP='$this->cep_endereco_cliente', 
		BAIRRO='$this->bairro_endereco_cliente',
		CIDADE='$this->cidade_endereco_cliente', 
		ESTADO='$this->estado_endereco_cliente',
		TIPO_CLIENTE='$this->tipo_cliente' where EMAIL = '$this->email_cliente'";
		if($this->banco->pesquisarBD($altera)){
			return true; // cliente foi alterado
			$this->banco->fechar();
		}
		return false; // erro na altera��o do cliente
		$this->banco->fechar();
	}
	
	function excluirCliente(){	
		$excluir = "delete from ".TABELA_CLIENTES." where EMAIL='$this->email-cliente'";
		if($this->banco->pesquisarBD($excluir)){
			return true; // cliente foi excluido
			$this->banco->fechar();
		}
		return false; // erro na exclus�o do cliente
		$this->banco->fechar();
	}
}
/*---------------------------------------------------------------------------------------------------
Classe cliente pessoa juridica derivada da classe clientes
---------------------------------------------------------------------------------------------------*/
class cliente_pj extends cliente{
	
	var $id_cliente_pj;
	var $razao_social_cliente_pj;
	var $nome_contato_cliente_pj;
	var $cnpj_cliente_pj;
	var $ie_cliente_pj;

	function cliente_pj($email='',$senha='')
	{
		parent::cliente($email,$senha);
		$consulta = "select * from ".TABELA_CLIENTES_PJ." where COD_CLIENTE_PJ='$this->id_cliente'";
		$resultado = $this->banco->pesquisarBD($consulta);
		if($dados = $this->banco->mostra_registros($resultado)){
			$this->id_cliente_pj = $dados["COD_CLIENTE_PJ"];
			$this->razao_social_cliente_pj = $dados["NOME"];
			$this->nome_contato_cliente_pj = $dados["RESPONSAVEL"];
			$this->cnpj_cliente_pj = $dados["CNPJ"];
			$this->ie_cliente_pj = $dados["IE"];
			return true;
		}
		return false;	
	}
	
	function cadastrarCliente()
	{
		parent::cadastrarCliente();
		$insere = "insert into ".TABELA_CLIENTES_PJ." 
		(COD_CLIENTE_PJ, NOME, RESPONSAVEL, 
		CNPJ, IE)
		values
		('$this->novaChave', '$this->razao_social_cliente_pj', '$this->nome_contato_cliente_pj', 
		'$this->cnpj_cliente_pj', '$this->ie_cliente_pj')";
		if($this->banco->pesquisarBD($insere)){
			return true; // cliente juridico foi cadastrado
			$this->banco->fechar();
		}
		return false; // erro na altera��o do cliente juridico
		$this->banco->fechar();
	}
	
	function alterarCliente(){
		parent::alterarCliente();
		$altera = "update ".TABELA_CLIENTES_PJ." set 
		NOME='$this->razao_social_cliente_pj', 
		RESPONSAVEL='$this->nome_contato_cliente_pj', 
		CNPJ='$this->cnpj_cliente_pj', IE='$this->ie_cliente_pj'
		where COD_CLIENTE_PJ = '$this->id_cliente_pj'";
		if($this->banco->pesquisarBD($altera)){
			return true; // cliente juridico foi alterado
			$this->banco->fechar();
		}
		return false; // erro na altera��o do cliente juridico
		$this->banco->fechar();
	}
	
	function excluirCliente(){	
		parent::excluirCliente();
		$excluir = "delete from ".TABELA_CLIENTES_PJ." where COD_CLIENTE_PJ = '$this->id_cliente_pj'";
		if($this->banco->pesquisarBD($altera)){
			return true; // cliente juridico foi excluido
			$this->banco->fechar();
		}
		return false; // erro na exclus�o do cliente juridico
		$this->banco->fechar();
	}		 
}
/*---------------------------------------------------------------------------------------------------
Classe cliente pessoa fisica derivada da classe clientes
---------------------------------------------------------------------------------------------------*/
class cliente_pf extends cliente{
	
	var $id_cliente_pf;
	var $nome_cliente_pf;
	var $cpf_cliente_pf;
	var $rg_cliente_pf;
	var $sexo_cliente_pf;
	var $data_nascimento_cliente_pf;

	function getDataNascimento($data){
		$this->data_nascimento_cliente_pf = converteDataBanco($data);
	}

	function cliente_pf($email='',$senha=''){
		parent::cliente($email,$senha);
		$consulta = "select * from ".TABELA_CLIENTES_PF." where COD_CLIENTE_PF='$this->id_cliente'";
		$resultado = $this->banco->pesquisarBD($consulta);
		if($dados = $this->banco->mostra_registros($resultado)){
			$this->id_cliente_pf = $dados["COD_CLIENTE_PF"];		
			$this->nome_cliente_pf = $dados["NOME"];
			$this->cpf_cliente_pf = $dados["CPF"];
			$this->rg_cliente_pf = $dados["IDENTIDADE"];
			$this->sexo_cliente_pf = $dados["SEXO"];
			$this->data_nascimento_cliente_pf = $dados["DATA_NASCIMENTO"];
			return false;
		}
		return false;
	}
			
	function setDataNascimento($data){
		$this->data_nascimento_cliente_pf = converteDataUsuario($data);
	}
	
	function cadastrarCliente()
	{
		parent::cadastrarCliente();
		$insere = "insert into ".TABELA_CLIENTES_PF." 
		(COD_CLIENTE_PF, NOME, CPF, 
		IDENTIDADE, SEXO, DATA_NASCIMENTO)
		values
		('$this->novaChave', '$this->nome_cliente_pf', '$this->cpf_cliente_pf', 
		'$this->rg_cliente_pf', '$this->sexo_cliente_pf', '$this->data_nascimento_cliente_pf')";
		if($this->banco->pesquisarBD($insere)){
			return true; // cliente fisico foi cadastrado
			$this->banco->fechar();
		}
		return false; // erro na altera��o do cliente fisico
		$this->banco->fechar();
	}
	
	function alterarCliente(){
		parent::alterarCliente();
		$altera = "update ".TABELA_CLIENTES_PF." set 
		NOME='$this->nome_cliente_pf', 
		CPF='$this->cpf_cliente_pf', 
		IDENTIDADE='$this->rg_cliente_pf', SEXO='$this->sexo_cliente_pf', DATA_NASCIMENTO='$this->data_nascimento_cliente_pf'
		where COD_CLIENTE_PF = '$this->id_cliente_pf'";
		if($this->banco->pesquisarBD($altera)){
			return true; // cliente fisico foi alterado
			$this->banco->fechar();
		}
		return false; // erro na altera��o do cliente fisico
		$this->banco->fechar();
	}
	
	function excluirCliente(){	
		parent::excluirCliente();
		$excluir = "delete from ".TABELA_CLIENTES_PF." where COD_CLIENTE_PF = '$this->id_cliente_pf'";
		if($this->banco->pesquisarBD($altera)){
			return true; // cliente fisico foi excluido
			$this->banco->fechar();
		}
		return false; // erro na exclus�o do cliente fisico
		$this->banco->fechar();
	}		 
}
?>