<?php 
include_once "BD.php";
define(maximo,10);
error_reporting(0);
class paginacao{
	var $arquivo;
	var $campos = array(); 
	var $valor_campo = array(); 
	var $id;
	var $maxpag;
	var $maxlink; 
	var $parametro;
	var $conexaoBD;
	var $paginas;
	var $resultado;
	var $resultado2;
	var $reg_inicial;
	var $reg_final;
	var $banco;
	var $args;
	var $pg_anterior;
	var $pg_atual;
	var $pg_proxima; 
	var $temp;
	var $lnk_impressos;
	var $especial = '';
	
function paginacao($arquivo,$tabela,$maxpag,$maxlink){
	$this->arquivo = $arquivo;
	for($cont=0;$cont<maximo;$cont++)	$this->campos[$cont] = 0;
	for($cont=0;$cont<maximo;$cont++)	$this->valor_campo[$cont] = 0;
	$this->id = 0;
	$this->tabela = $tabela;
	$this->maxpag = $maxpag;
	$this->maxlink = $maxlink;
	$this->banco = new BD();
	$this->conexaoBD = $this->banco->obterConexao(); 
}

function get_campos(){
	$this->args = func_num_args();
	$this->campos = func_get_args();
}

function obterResultado(){
	$indice = array();
	$args = func_num_args();
	$this->valor_campo = func_get_args();
	if ($this->id == ''){
		$this->parametro = 0;
	} 
	else { 
    	$this->temp = $this->id; 
        $passo1 = $this->temp - 1; 
        $passo2 = $passo1*$this->maxpag; 
        $this->parametro = $passo2;
	}
	$cont2=0;
	for($cont=0;$cont<$args;$cont++){
		if($this->valor_campo[$cont]!=''){
			$this->valor_campo[$cont]=$this->valor_campo[$cont];
			$indice[$cont2]=$cont;
			$cont2++;
		}
	}
	if($cont2==0){
		if($this->especial==''){
			$sql = "select * from $this->tabela";
			$sql2 = "select * from $this->tabela limit $this->parametro,$this->maxpag";
		}
		$sql = "select * from $this->tabela ".$this->especial;
		$sql2 = "select * from $this->tabela ".$this->especial." limit $this->parametro,$this->maxpag";
	}
	else {
		$sql = "select * from $this->tabela where";	
		for($cont=0;$cont<$cont2;$cont++){
			$sql .= " ".$this->campos[$indice[$cont]]." like ('%'\"".$this->valor_campo[$indice[$cont]]."\"'%')";
			if($indice[$cont+1]) 
			$sql .= " and";
		}
		if($this->especial==''){
			$sql2 = $sql." limit $this->parametro,$this->maxpag";
		}
		else{
			$sql2 = $sql.$this->especial." limit $this->parametro,$this->maxpag";
		}
	}
	$this->resultado = $this->banco->pesquisarBD($sql); 
    $this->resultado2 = $this->banco->pesquisarBD($sql2);
	$totreg = $this->banco->total_registros($this->resultado);
    $totreg2 = $this->banco->total_registros($this->resultado2);
	$this->results_tot = $totreg; 
    $results_parc = $totreg2; 
    $result_div = $this->results_tot/$this->maxpag; 
    $n_inteiro = (int)$result_div; 
    if ($n_inteiro < $result_div) {$this->paginas = $n_inteiro + 1;} 
    else {$this->paginas = $result_div;} 
    $this->pg_atual = $this->parametro/$this->maxpag+1; 
    $this->reg_inicial = $this->param + 1; 
    $this->pg_anterior = $this->pg_atual - 1; 
    $this->pg_proxima = $this->pg_atual + 1;
	return $sql2;
}

function exibirResultado($campo){
	static $cont =0;
	if($result = $this->banco->mostra_registros($this->resultado2)){
		$cont++;
		return $result;
	}
	$this->reg_final = $this->parametro + $cont;
	return false;
}

function anterior(){
	$url = $this->arquivo."?id=".$this->pg_anterior;
	if($this->id>0){
		for($cont=0;$cont<$this->args;$cont++){
			$url .= "&".$this->campos[$cont]."=".$this->valor_campo[$cont];
		}
	}
	return $url;
}

function links(){
	$link;
	$link = $this->pg_atual." de ".$this->paginas;
	return $link;
}

/*function calculaLinks(){
	if ($this->temp >= $this->maxlnk){ 
    	if ($this->paginas > $this->maxlnk) {$n_maxlnk = $this->temp + 4; 
            $this->maxlnk = $n_maxlnk; 
            $n_start = $this->temp - 6; 
            $this->lnk_impressos = $n_start;
		}
	} 
    while(($this->lnk_impressos < $this->paginas) and ($this->lnk_impressos < $this->maxlnk)) 
		{
		$this->lnk_impressos ++;
}

function exibirLinks(){
	if ($this->pg_atual != $this->lnk_impressos){
		$link="<a href=\"".$arquivo"?id=".$this->lnk_impressos."&".materia=$dentistica\" class=\"estilos\">";
	} 
    if ($this->pg_atual == $this->lnk_impressos){
		$link="<h2>$lnk_impressos<h2>";
	}
	else {
		$link="$lnk_impressos";
	}
	return $link;
}*/

function proxima(){
	$url = $this->arquivo."?id=".$this->pg_proxima;
	if($this->reg_final<$this->results_tot){
		for($cont=0;$cont<$this->args;$cont++){
			$url .= '&'.$this->campos[$cont].'='.$this->valor_campo[$cont];
		}
	}
	return $url;
}
}
?>