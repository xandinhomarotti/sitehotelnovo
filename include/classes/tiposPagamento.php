<?php
include_once "include/funcoesUteis.php";
include_once "include/classes/BD.php";
//_________________________________________________________________________________________________
// definindo as tabelas relacionadas a tipos de pagamentos
//_________________________________________________________________________________________________
define('TABELA_PAGAMENTOS','pagamentos');
define('TABELA_ACESSOS','acessos');
define('TABELA_BOLETOS','boletos');

/*---------------------------------------------------------------------------------------------------
Classe tipos de pagamento
---------------------------------------------------------------------------------------------------*/
class tiposPagamento{
	
	var $cod_pagamento;
	var $tipo_pagamento;
	var $descricao;
	var $ativo;
	var $data_operacao;
	var $hora_operacao;
	var $cod_acesso;
	var $exibicao;
	var $sql;

	function tiposPagamento($cod_pagamento=''){
		$banco = new BD;
		$consulta = "select * from ".TABELA_PAGAMENTOS." where COD_PAGAMENTO='$cod_pagamento'";
		$resultado = $banco->pesquisarBD($consulta);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$this->cod_pagamento = $dados["COD_PAGAMENTO"];
			$this->tipo_pagamento = $dados["TIPO_PAGAMENTO"];
			$this->ativo = $dados["ATIVO"];
			$this->descricao = $dados["DESCRICAO"];			
			$this->data_operacao = $dados["DATA_OPERACAO"];
			$this->hora_operacao = $dados["HORA_OPERACAO"];
			$this->cod_acesso = $dados["COD_ACESSO"];
		}
		$this->cod_pagamento = $cod_pagamento;
	}
	
	function verificaCadastroTipoPagamento($tipo_pagamento){
		$banco = new BD;
		$verifica = "select * from ".TABELA_PAGAMENTOS." where TIPO_PAGAMENTO = '$tipo_pagamento'";
		$resultado = $banco->pesquisarBD($verifica);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$banco->fechar();
			return true; // o tipo de pagamento ja est� cadastrado
		}
		$banco->fechar();
		return false;
	}	
	
	function cadastrarTipoPagamento()
	{
		$banco = new BD;	
		$this->data_operacao = converteDataUsuario(date('d/m/Y'));
		$this->hora_operacao = date('H:i:s');	
		$insere = "insert into ".TABELA_PAGAMENTOS." 
		(TIPO_PAGAMENTO, ATIVO, DATA_OPERACAO, HORA_OPERACAO, COD_ACESSO)values
		('$this->tipo_pagamento', '$this->ativo', '$this->data_operacao', '$this->hora_operacao', '$this->cod_acesso')";
		$this->sql = $insere;
		if($banco->pesquisarBD($insere)){
			$banco->fechar();
			return true; // tipo de pagamento foi cadastrado
		}
		$banco->fechar();
		return false; // erro no cadastro de tipo de pagamento
	}
	
	function alterarTipoPagamento()
	{
		$banco = new BD;	
		$altera = "update ".TABELA_PAGAMENTOS." set TIPO_PAGAMENTO='$this->tipo_pagamento',ATIVO='$this->ativo' where 
		COD_PAGAMENTO = '$this->cod_pagamento'";
		if($banco->pesquisarBD($altera)){
			$banco->fechar();
			return true; // tipo de pagamento foi alterado
		}
		$banco->fechar();
		return false; // erro na altera��o do tipo de pagamento
	}
	
	function excluirTipoPagamento(){
		$banco = new BD;	
		$excluir = "delete from ".TABELA_PAGAMENTOS." where COD_PAGAMENTO='$this->cod_pagamento'";
		if($banco->pesquisarBD($excluir)){
			$banco->fechar();
			return true; // tipo de pagamento foi excluido
		}
		$banco->fechar();
		return false; // erro na exclus�o do tipo de pagamento
	}

	function carregarExibicaoTiposPagamento($qtd=''){
		$banco = new BD();
		$tudo = "select * from ".TABELA_PAGAMENTOS;
		$resultado = $banco->pesquisarBD($tudo);
		$final = $banco->total_registros($resultado);
		if($final==1) $inicio=0;
		if($qtd!=''){
			$inicio = $final - $qtd;
			$consulta = "select * from ".TABELA_PAGAMENTOS." where ATIVO='S' order by TIPO_PAGAMENTO limit ".$inicio.",".$final;
		}
		else $consulta = "select * from ".TABELA_PAGAMENTOS." where ATIVO='S' order by DESCRICAO";
		$this->exibicao = $banco->pesquisarBD($consulta);
		$this->sql = $consulta;
	}
	
	function exibirTiposPagamento(){
		$banco = new BD();
		return $banco->mostra_registros($this->exibicao);
	}
}
/*---------------------------------------------------------------------------------------------------
Classe boleto derivada da classe tiposPagamento
---------------------------------------------------------------------------------------------------*/
class boleto extends tiposPagamento{

	var $cod_boleto;
	var $banco;
	var $imagem;
	var $dias_para_pagamento;
	var $taxa_boleto;
	var $demonstrativo;
	var $instrucoes;
	var $agencia;
	var $conta;
	var $digito_verificador;
	var $carteira;
	var $cod_cliente_banco;
	var $especie_doc;
	var $especie;
	var $ativo;
	var $data_operacao;
	var $hora_operacao;
	var $cod_acesso;
 
	function boleto($cod_boleto=''){
		$banco = new BD;
		$consulta = "select * from ".TABELA_BOLETOS." where 
		COD_BOLETO='$cod_boleto'";
		$resultado = $banco->pesquisarBD($consulta);
		$dados = $banco->mostra_registros($resultado);
		if($dados){
			$this->cod_boleto = $dados["COD_BOLETO"];
			$this->banco = $dados["BANCO"];
			$this->imagem = $dados["IMAGEM"];
			$this->dias_para_pagamento = $dados["DIAS_PARA_PAGAMENTO"];
			$this->taxa_boleto = $dados["TAXA_BOLETO"];
			$this->demonstrativo = $dados["DEMONSTRATIVO"];
			$this->instrucoes = $dados["INSTRUCOES"];
			$this->agencia = $dados["AGENCIA"];
			$this->conta = $dados["CONTA"];
			$this->digito_verificador = $dados["DIGITO_VERIFICADOR"];
			$this->carteira = $dados["CARTEIRA"];
			$this->cod_cliente_banco = $dados["COD_CLIENTE_BANCO"];
			$this->especie_doc = $dados["ESPECIE_DOC"];
			$this->especie = $dados["ESPECIE"];
			$this->ativo = $dados["ATIVO"];			
			$this->data_operacao = $dados["DATA_OPERACAO"];
			$this->hora_operacao = $dados["HORA_OPERACAO"];
			$this->cod_acesso = $dados["COD_ACESSO"];
			//parent::tipoPagamento($this->cod_pagamento);		
		}
		$this->cod_boleto = $cod_boleto;
		$banco->fechar();
	}
/*	function carregarExibicaoBoletos($qtd=''){
		$banco = new BD();
		$tudo = "select * from ".TABELA_BOLETOS;
		$resultado = $banco->pesquisarBD($tudo);
		$final = $banco->total_registros($resultado);
		if($final==1) $inicio=0;
		if($qtd!=''){
			$inicio = $final - $qtd;
			$consulta = "select * from ".TABELA_BOLETOS." where ATIVO='S' order by BANCO limit ".$inicio.",".$final;
		}
		else $consulta = "select * from ".TABELA_BOLETOS." where ATIVO='S' order by BANCO";
		$this->exibicao = $banco->pesquisarBD($consulta);
		$this->sql = $consulta;
	}
	
	function exibirBoletos(){
		$banco = new BD();
		return $banco->mostra_registros($this->exibicao);
	}*/
}
?>