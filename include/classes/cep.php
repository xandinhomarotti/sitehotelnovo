<?php
include_once "BD.php";

class cep{

	var $cep;
	var $endereco;
	var $bairro;
	var $cidade;
	var $uf;
	var $logradouro;
	var $banco;
	var $tabela = "cep";
		
	function cep($cep=''){
		$this->banco = new BD;
		$consulta = "select * from $this->tabela where CEP='$cep'";
		$resultado = $this->banco->pesquisarBD($consulta);
		$dados = $this->banco->mostra_registros($resultado);
		if($dados){
		$this->cep = $dados["CEP"];
		$this->endereco = $dados["ENDERECO"];
		$this->bairro = $dados["BAIRRO"];
		$this->cidade = $dados["CIDADE"];
		$this->uf = $dados["UF"];
		$this->logradouro = $dados["LOGRADOURO"];
		}
	}
	
	function getLogradouro(){
		$dados = array();
		$consulta = "select distinct LOGRADOURO from cep order by LOGRADOURO";
		$logradouro = $this->banco->pesquisarBD($consulta);
		while($temp = $this->banco->mostra_registros($logradouro)){
			array_push($dados,$temp["LOGRADOURO"]);
		}
		return $dados;
	}
	
	function getUF(){
		$dados = array();
		$consulta = "select distinct UF from cep order by UF";
		$logradouro = $this->banco->pesquisarBD($consulta);
		while($temp = $this->banco->mostra_registros($logradouro)){
			array_push($dados,$temp["UF"]);
		}
		return $dados;
	}
	
	function consultarEndereco($cep){
	$consulta = "select * from cep where CEP='$cep'";
	$endereco = $this->banco->pesquisarBD($consulta);
	if($dados = $this->banco->mostra_registros($endereco)){
		$this->cep = $dados["CEP"];
		$this->endereco = $dados["ENDERECO"];
		$this->bairro = $dados["BAIRRO"];
		$this->cidade = $dados["CIDADE"];
		$this->uf = $dados["UF"];
		$this->logradouro = $dados["LOGRADOURO"];
		return true;
	}
	else{
		return false;
	}
	}
	
	function verificaCep($cep){
		$consulta = "select * from cep where CEP='$cep'";
		$endereco = $this->banco->pesquisarBD($consulta);
		if($dados = $this->banco->mostra_registros($endereco)){
			return true;
		}
		else{
			return false;
		}
	}
}	
?>