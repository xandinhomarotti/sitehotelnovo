<?php
include_once "include/classes/acessos.php";
include_once "include/classes/tiposPagamento.php";
include_once "include/classes/BD.php";
include_once "include/classes/cep.php";
include_once "include/classes/clientes.php";
include_once "include/classes/email.php";
include_once "include/classes/paginacao.php";
include_once "include/classes/pedidos.php";
include_once "include/classes/produtos.php";
include_once "include/classes/new.php";
session_start();
include_once "include/funcoesUteis.php";
include_once "include/iniciaSite.php";
$banco = new BD();
$sql = "select * from empresa where cod_empresa='1'";
$resultado = $banco->pesquisarBD($sql);
$dados = $banco->mostra_registros($resultado);
define('NOME_SITE',$dados["nome"]);
define('SLOGAN',$dados["slogan"]);
define('TELEFONE',$dados["telefone"]);
define('EMAIL',$dados["email"]);
define('RODAPE',$dados["rodape"]);
define('TITULO_PAGINA',NOME_SITE.' - '.SLOGAN);
if(!session_is_registered("fabricante")){
	session_register("fabricante");
	$fabricante= new fabricante();
}
if(!session_is_registered("produto")){
session_register("produto");
$produto = new produto();
}
?>